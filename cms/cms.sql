create database qt_pratice;
drop table t_user;
CREATE table t_user
(
    id bigserial primary key,
    name        VARCHAR(24) not null DEFAULT '',
    age         int         not null DEFAULT 0,
    gender      BOOLEAN     not null DEFAULT FALSE,
    phone_no    varchar(16) not null DEFAULT '',
    email       varchar(32) not null default '',
    level       char(2)     not null default 'p1',
    roles       varchar(16) not null default '01',
    up_id       bigint      not null DEFAULT 0,
    create_time timestamp   not null DEFAULT CURRENT_TIMESTAMP,
    del_flag    BOOLEAN     not null DEFAULT FALSE
);

drop table t_roles;
create table t_roles
(
    id          int         not null primary key,
    roles       char(2)     not null DEFAULT '',
    description VARCHAR(16) not null DEFAULT '',
    create_time TIMESTAMP   not null DEFAULT CURRENT_TIMESTAMP,
    del_flag    BOOLEAN     not null DEFAULT FALSE
)

