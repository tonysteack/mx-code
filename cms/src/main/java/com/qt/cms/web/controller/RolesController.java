/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.qt.cms.web.controller;

import com.qt.cms.entity.Roles;
import com.qt.cms.web.service.RolesService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author tao.qu
 * @version 1.0
 **/
@RestController
@AllArgsConstructor
public class RolesController {
    private final RolesService rolesService;

    @GetMapping("/roles")
    public List<Roles> listAllRoles() {
        return rolesService.listRoles();
    }

    @PostMapping("insert")
    public int insertRole(@RequestBody Roles roles) {
        return rolesService.insert(roles);
    }

    @GetMapping("hello")
    public String hello() {
        return "hello";
    }
}
