/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.qt.cms.web.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qt.cms.entity.Roles;
import com.qt.cms.repository.RolesRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tao.qu
 * @version 1.0
 **/
@Service
@AllArgsConstructor
@Slf4j
public class RolesService {
    private final RolesRepository rolesMapper;

    public int insert(Roles roles) {
        log.info("insert roles={}", JSON.toJSONString(roles));
        return rolesMapper.insert(roles);
    }

    public int update(Roles roles) {
        log.info("update roles{}",JSON.toJSONString(roles));
        return rolesMapper.updateById(roles);
    }

    public List<Roles> listRoles() {
        return rolesMapper.selectList(new QueryWrapper<>());
    }
}
