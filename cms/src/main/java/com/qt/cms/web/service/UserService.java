/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.qt.cms.web.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qt.cms.entity.User;
import com.qt.cms.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tao.qu
 * @version 1.0
 **/
@Service
@Slf4j
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public int insert(User user) {
        log.info("insert user={}", JSON.toJSONString(user));
        return userRepository.insert(user);
    }

    public int update(User user) {
        log.info("update user={}", JSON.toJSONString(user));
        return userRepository.updateById(user);
    }

    public List<User> listAll() {
        return userRepository.selectList(new QueryWrapper<>());
    }
}
