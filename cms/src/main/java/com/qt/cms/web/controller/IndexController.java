/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.qt.cms.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author tao.qu
 * @version 1.0
 **/
@Controller
public class IndexController {

    @RequestMapping("index")
    public String index() {
        return "index";
    }
}
