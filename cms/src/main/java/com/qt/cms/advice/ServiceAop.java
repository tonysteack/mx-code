/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.qt.cms.advice;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

/**
 * @author tao.qu
 * @version 1.0
 **/
@Service
@Aspect
@Slf4j
public class ServiceAop {

    @Pointcut("execution(* com.qt.cms.web.service..*.*(..))*")
    private void servicePoint() {}

    @Around("servicePoint()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        StopWatch watch = new StopWatch();
        watch.start();
        log.info("******************* service methodName{},args{}",joinPoint.getSignature().getName(),joinPoint.getArgs());
        watch.stop();
        Object result = joinPoint.proceed(joinPoint.getArgs());
        log.info("service handler cost {}ms,result{}", watch.getTotalTimeMillis(), result);
        return result;
    }
}
