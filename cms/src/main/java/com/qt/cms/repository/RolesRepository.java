/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.qt.cms.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qt.cms.entity.Roles;

/**
 * @author tao.qu
 * @version 1.0
 **/
public interface RolesRepository extends BaseMapper<Roles> {
}
