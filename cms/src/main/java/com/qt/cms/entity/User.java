/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.qt.cms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;

/**
 * @author tao.qu
 * @version 1.0
 **/
@TableName("t_user")
@Data
public class User {
    @Id
    private Long id;

    private String name;

    private int age;

    /**
     * false 男 true 女
     */
    private boolean gender;

    private String phoneNo;

    private String email;

    private String roles;

    private Long upId;

    private Date createTime;

    private boolean delFlag;
}
