/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.qt.cms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;

/**
 * @author tao.qu
 * @version 1.0
 **/
@TableName("t_roles")
@Data
public class Roles {
    @Id
    private Integer id;

    @TableField("roles")
    private String roles;

    @TableField("description")
    private String description;

    @TableField("create_time")
    private Date createTIme;

    @TableField("del_flag")
    private boolean delFlag;
}
