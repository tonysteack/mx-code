package com.tonysteack.integration;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.meditrusthealth.fast.universal.service.db.entity.UniversalUserWelfareConfig;
import com.tonysteack.integration.bean.DrugDiseaseModel;
import com.tonysteack.integration.mapper.UniversalDrugDiseaseMappingMapper;
import com.tonysteack.integration.mapper.UniversalUserWelfareConfigMapper;
import com.tonysteack.integration.test.HandlerFactory;
import com.tonysteack.integration.utils.ExcelListener;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

@SpringBootTest
class TestApplicationTests {
    @Resource
    private HandlerFactory handlerFactory;

    @Resource
    private UniversalDrugDiseaseMappingMapper mappingMapper;

    @Resource
    private UniversalUserWelfareConfigMapper configMapper;

    @Test
    void contextLoads() {
    }

    @Test
    void test() {
        handlerFactory.of("event").handler("hello world");
    }

    @Test
    public void testExcelImport() throws FileNotFoundException {
        String filePath = "C:\\Users\\tao.qu\\Desktop\\2022年药康复销售过的药品_0331.xlsx";
        File excel = new File(filePath);
        ExcelReader reader = EasyExcel.read(new FileInputStream(excel), DrugDiseaseModel.class, new ExcelListener(mappingMapper)).build();
        ReadSheet sheet = EasyExcel.readSheet(0).build();
        reader.read(sheet);
        reader.finish();
    }

    @Test
    public void testSelect() {
        List<UniversalUserWelfareConfig> configList = configMapper.selectAll();
        configList.stream().map(config-> {
            String relatedDrug = config.getRelatedDrug();
            String relatedDisease = config.getRelatedDisease();
            relatedDrug = relatedDrug.replaceAll("'","\"");
            relatedDisease = relatedDisease.replaceAll("'","\"");
            config.setRelatedDrug(relatedDrug);
            config.setRelatedDisease(relatedDisease);
            if (StringUtils.isBlank(config.getDrugName())) {
                config.setDrugName("");
            }
            if (StringUtils.isBlank(config.getProjectName())) {
                config.setProjectName("");
            }
            configMapper.updateByPrimaryKeySelective(config);
            return config;
        }).forEach(System.out::println);
    }


    public static void main(String[] args) {
        String str ="[{'diseaseName': '肺癌', 'diseaseCode': 7}]";
        str = str.replaceAll("'","\"");
        System.out.println(str);
    }
}
