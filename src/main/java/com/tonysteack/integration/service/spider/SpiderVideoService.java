package com.tonysteack.integration.service.spider;

public interface SpiderVideoService {
    /**
     * 根据关键词搜索电影资源
     * @param name
     */
    void getVideo(String name);
}
