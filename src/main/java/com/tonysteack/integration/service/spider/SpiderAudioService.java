package com.tonysteack.integration.service.spider;

public interface SpiderAudioService {
    void getAudio(String name);
}
