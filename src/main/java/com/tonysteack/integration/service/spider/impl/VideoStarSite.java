package com.tonysteack.integration.service.spider.impl;

import com.tonysteack.integration.service.spider.SpiderVideoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 星辰影院
 * http://www.94069.net
 */
@Service
@Slf4j
public class VideoStarSite implements SpiderVideoService {

    @Override
    public void getVideo(String name) {

    }
}
