package com.tonysteack.integration.service.spider;

public interface SpiderImageService {
    void getImage(String name);

}
