package com.tonysteack.integration.service.spider.impl;

import com.tonysteack.integration.annotations.Value;
import com.tonysteack.integration.service.spider.SpiderVideoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * https://5ji.tv/
 * 无极电影网
 */
@Service
@Slf4j
public class VideoWujiSite implements SpiderVideoService {
    @Value("${videosite.wuji}")
    private String wujiSite;
    @Override
    public void getVideo(String name) {
        String url = wujiSite + "search.html?wd=" + name;

    }
}
