package com.tonysteack.integration.service.spider.impl;

import com.tonysteack.integration.service.spider.SpiderVideoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 农民影视
 * https://www.lywcdp.com
 */
@Service
@Slf4j
public class VideoFarmerSite implements SpiderVideoService {

    @Override
    public void getVideo(String name) {

    }
}
