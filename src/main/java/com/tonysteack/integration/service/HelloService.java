/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.service;

import com.tonysteack.integration.annotations.Value;
import org.springframework.stereotype.Service;

@Service
public class HelloService {
    @Value("helloservice")
    private static String helloservice = "helloservice";

    public String hello(String message) {
        System.out.println("hello");
        return message;
    }
}
