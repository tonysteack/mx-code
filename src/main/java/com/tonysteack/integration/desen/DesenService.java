/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.desen;

/**
 * @author tao.qu
 */
@FunctionalInterface
public interface DesenService {
    String desensiti(String text);
}
