/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.desen;

import java.util.regex.Pattern;

/**
 * 手机号脱敏
 */
public class MobileDesenImpl implements DesenService {
    @Override
    public String desensiti(String text) {
        if (text.equals("") || text.length() == 0) {
            throw new IllegalArgumentException("参数不能为空");
        }
        String phonePattern = "^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\\d{8}$";
        Pattern pattern = Pattern.compile(phonePattern);
        if (!pattern.matcher(text).matches()) {
            throw new IllegalArgumentException("手机号不合法");
        }
        String head = text.substring(0, 3);
        String end = text.substring(7);
        return head + "****" + end;
    }
}
