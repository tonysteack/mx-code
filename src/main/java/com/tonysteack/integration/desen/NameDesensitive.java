/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.desen;

import java.util.regex.Pattern;

/**
 * 用户姓名脱敏
 */
public class NameDesensitive implements DesenService{
    @Override
    public String desensiti(String text) {
        if (null == text || text.length()<2){
            throw new IllegalArgumentException("姓名不能为空或长度不够");
        }
        String namePattern = "^(?!.*\\..*\\.)[\\u4e00-\\u9fa5]([\\u4e00-\\u9fa5\\.]*|[A-Za-z\\.]*)$";
        if (!Pattern.matches(namePattern,text)) {
            throw new IllegalArgumentException("姓名格式不合法");
        }
        int len = text.length()-1;
        text = text.substring(0,1);
        for (int i = 0; i < len; i++) {
            text = text + "*";
        }
        return text;
    }
}
