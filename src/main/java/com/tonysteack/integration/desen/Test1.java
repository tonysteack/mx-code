/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.desen;

public class Test1 {
    private static void test(DesenService desenService, String text) {
        System.out.println(desenService.desensiti(text));
        desenService.desensiti(text);
    }

    public static void main(String[] args) {
        DesenService desen = new MobileDesenImpl();
        System.out.println(desen.desensiti("15864113525"));
        DesenService nameDesen = new NameDesensitive();
        System.out.println(nameDesen.desensiti("张三丰"));
        test((str) -> {
            return str + "蓝天白云。";
        }, "text");
    }
}
