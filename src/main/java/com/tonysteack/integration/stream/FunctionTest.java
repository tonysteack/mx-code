/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.stream;

import java.util.function.Function;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class FunctionTest {
    public static void main(String[] args) {
        Function<Integer, Integer> f1 = (r) -> r * r;
        Function<Integer, Integer> f2 = (r) -> r + r;
        System.out.println("********************apply*********************");
        System.out.println(apply(100, f1));
        System.out.println(apply(20, f2));
        System.out.println("********************compose*********************");
        System.out.println(compose(100, f1, f2));
        System.out.println(compose(20, f2, f1));
        System.out.println("******************andThen***********************");
        System.out.println(andThen(100, f1, f2));
        System.out.println(andThen(20, f2, f1));
    }

    public static Integer apply(Integer num, Function<Integer, Integer> function) {
        return function.apply(num);
    }

    /**
     * 先执行f2，再用f2的执行结果作为f1的执行参数
     *
     * @param num
     * @param f1
     * @param f2
     * @return
     */
    public static Integer compose(Integer num, Function<Integer, Integer> f1, Function<Integer, Integer> f2) {
        return f1.compose(f2).apply(num);
    }

    /**
     * 先执行f1，再把f1的结果做为f2的参数
     * @param num
     * @param f1
     * @param f2
     * @return
     */
    public static Integer andThen(Integer num, Function<Integer, Integer> f1, Function<Integer, Integer> f2) {
        return f1.andThen(f2).apply(num);
    }
}
