/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.stream;

import java.util.function.Consumer;

/**
 * 消费型函数接口
 *
 * @author tao.qu
 * @version 1.0
 **/
public class ConsumerTest {
    private static StringBuffer sb = new StringBuffer();
    private static Consumer<String> consumer = (str) -> System.out.println("str:" + str + ",length=" + str.length());
    private static Consumer<String> c1 = (str) -> sb.append(str);
    private static Consumer<String> c2 = (str) -> sb.append(str + "\n");


    public static void main(String[] args) {
        //调用accept方法，无返回体。
        consumer.accept("hello world!");

        //先执行调用者的accept方法，在用同样的参数执行做为andThen参数的匿名函数。
        consumer.andThen((str) -> System.out.println("str" + str)).accept("hello world");
        System.out.println("****************************************************");
        c1.accept("hello world");
        System.out.println(sb.toString());
        c1.andThen(c2).accept("lucky boy!");
        System.out.println(sb.toString());
        System.out.println("******************************************************");
        String[] arrs = {"张三,男","李四,男","范冰冰,女"};
        for (int i = 0; i < arrs.length; i++) {
            printInfo(arrs[i], (msg) -> {
                System.out.print("姓名：" + msg.split(",")[0]);
            }, (msg) -> {
                System.out.println("   性别：" + msg.split(",")[1]);
            });
        }
    }

    public static void printInfo(String message, Consumer<String> c1, Consumer<String> c2) {
        c1.andThen(c2).accept(message);
    }
}
