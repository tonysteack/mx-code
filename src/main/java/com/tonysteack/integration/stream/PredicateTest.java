/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.stream;

import com.tonysteack.integration.design.observe.User;

import java.util.function.Predicate;

/**
 * 判断型函数式接口测试类
 * 方法  函数数量  关系  总结
 * and   2        并   两个函数都必须同时为true才返回true
 * or    2        或   两个函数最少要有一个为true才返回true
 *negate 1       否   取反
 *
 * @author tao.qu
 * @version 1.0
 **/
public class PredicateTest {

    private static Predicate<Integer> isOddNum = (num) -> num%2 != 0;
    private static Predicate<Integer> isEvenNum = (num) -> num%2 == 0;
    private static Predicate<User> isMale = user -> user.getGender().equals("男");
    private static Predicate<User> isOlderThan20 = user -> user.getAge() > 20;

    public static void main(String[] args) {
        System.out.println(isOddNum.test(18));
        System.out.println(isEvenNum.test(18));

        System.out.println("********************and************************");
        System.out.println(isOddNum.and(isEvenNum).test(18));
        User user = new User("张三", "男", 18);
        System.out.println(isMale.and(isOlderThan20).test(user));
        User user1 = new User("lisi", "男", 28);
        System.out.println(isMale.and(isOlderThan20).test(user1));

        System.out.println("********************or************************");
        User user2 = new User("王五", "男", 18);
        System.out.println(isMale.or(isOlderThan20).test(user2));
        User user3 = new User("lisi", "女", 17);
        System.out.println(isMale.or(isOlderThan20).test(user3));

        System.out.println("********************negate************************");
        User user4 = new User("马六", "男", 18);
        System.out.println(isMale.negate().test(user4));
        User user5 = new User("赵7", "女", 17);
        System.out.println(isOlderThan20.negate().test(user5));

        System.out.println("********************isEqual************************");
        String str = "hello world!";
        Predicate<Object> predicate = Predicate.isEqual(str);
        System.out.println(predicate.test("hello world!"));
        System.out.println(predicate.test("hello world"));
    }


}
