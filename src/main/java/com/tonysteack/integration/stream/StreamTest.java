/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.stream;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Stream常用功能的测试案列集合
 * @author tao.qu
 * @version 1.0
 **/
public class StreamTest {
    //将数组转为list
    private static List<String> arrToList(String[] strs) {
//        return Arrays.asList(strs);
//        return Arrays.stream(strs).collect(Collectors.toList());
        return Stream.of(strs).collect(Collectors.toList());
    }

    /**
     *  创建流的几种方式
     *  list---->stream
     *  set----->stream
     *  map----->stream
     */
    private static void createStream() {
        new ArrayList<>().stream();
        new HashSet<String>().stream();

    }

}
