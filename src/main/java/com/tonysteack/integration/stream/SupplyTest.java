/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.stream;

import java.util.function.Supplier;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class SupplyTest {
    private static Supplier<Integer> supply = () -> 18;

    public static void main(String[] args) {
        System.out.println(supply.get());
        System.out.println(add(18,12).get());
        String[] arrs = {"h","hello","hello world"};
        System.out.println(maxLength(arrs).get());
    }

    public static Supplier<Integer> add(Integer num1, Integer num2) {
        return () -> num1 + num2;
    }

    public static Supplier<Integer> maxLength(String[] args) {
        final int[] len = {0};
        return () -> {
            for (int i = 0; i < args.length; i++) {
                if (args[i].length() > len[0]) {
                    len[0] = args[i].length();
                }
            }
            return len[0];
        };
    }
}
