/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.juc;

import com.tonysteack.integration.utils.SmallTools;

import java.util.concurrent.CompletableFuture;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class SupplyAsyncTest {

    public static void main(String[] args) {
//       async();
//        async1();
        async3();
    }

    /**
     * 厨师自己做饭 盛饭 小白吃饭
     */
    public static void async() {
        SmallTools.printTimeAndThread("小白进入餐厅，点了一份饭");
        CompletableFuture<String> cook = CompletableFuture.supplyAsync(() -> {
            SmallTools.printTimeAndThread("厨师炒菜");
            SmallTools.sleep(200);
            SmallTools.printTimeAndThread("厨师打饭");
            SmallTools.sleep(200);
            return "饭做好了";
        });
        SmallTools.printTimeAndThread("小白在打王者");
        //join()会等待任务执行结束 并返回任务结果
        SmallTools.printTimeAndThread(String.format("%s,小白开吃",cook.join()));

    }

    /**
     * 服务员盛饭 厨师炒菜，两个线程并行
     */
    public static void async1() {
        SmallTools.printTimeAndThread("小白进入餐厅，点了一份饭");
        CompletableFuture<String> rice = CompletableFuture.supplyAsync(() -> {
            SmallTools.printTimeAndThread("服务员打饭");
            SmallTools.sleep(200);
            CompletableFuture<String> cook = CompletableFuture.supplyAsync(() -> {
                SmallTools.printTimeAndThread("厨师炒菜");
                SmallTools.sleep(200);
                return "菜炒好了";
            });
            return String.format("%s,饭也盛好了",cook.join());
        });
        SmallTools.printTimeAndThread("小白在打王者");
        //join()会等待任务执行结束 并返回任务结果
        SmallTools.printTimeAndThread(String.format("%s,小白开吃",rice.join()));
    }

    public static void async2() {
        SmallTools.printTimeAndThread("小白进入餐厅，点了一份饭");
            CompletableFuture<String> cook = CompletableFuture.supplyAsync(() -> {
                SmallTools.printTimeAndThread("厨师炒菜");
                SmallTools.sleep(200);
                return "菜炒好了";
            }).thenCompose(dish -> CompletableFuture.supplyAsync(()-> {
                SmallTools.printTimeAndThread("服务员盛饭");
                SmallTools.sleep(100);
                return dish + "，饭盛好了";
            }));
        SmallTools.printTimeAndThread("小白在打王者");
        //join()会等待任务执行结束 并返回任务结果
        SmallTools.printTimeAndThread(String.format("%s,小白开吃",cook.join()));
    }

    public static void async3() {
        SmallTools.printTimeAndThread("小白进入餐厅，点了一份饭");
        CompletableFuture<String> cook = CompletableFuture.supplyAsync(() -> {
            SmallTools.printTimeAndThread("厨师炒菜");
            SmallTools.sleep(200);
            return "菜炒好了";
        }).thenCombine(CompletableFuture.supplyAsync(() -> {
            SmallTools.printTimeAndThread("服务券蒸米饭");
            SmallTools.sleep(500);
            return "米饭蒸好了";
        }), (dish, rice) -> {
          SmallTools.printTimeAndThread("服务员打饭");
          SmallTools.sleep(100);
          return String.format("%s + %s 好了",dish,rice);
        });

        SmallTools.printTimeAndThread("小白在打王者");
        //join()会等待任务执行结束 并返回任务结果
        SmallTools.printTimeAndThread(String.format("%s,小白开吃",cook.join()));
    }
}
