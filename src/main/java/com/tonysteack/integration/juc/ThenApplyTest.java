/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.juc;

import com.tonysteack.integration.utils.SmallTools;

import java.util.concurrent.CompletableFuture;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class ThenApplyTest {

    /**
     * 小白吃完饭开发票，接电话并回家
     *
     * @param args
     */
    public static void invoice() {
        SmallTools.printTimeAndThread("小白吃完饭了 结账并要求开发票");
        CompletableFuture<String> invoiced = CompletableFuture.supplyAsync(() -> {
            SmallTools.printTimeAndThread("服务员收款500元");
            SmallTools.sleep(60);
            SmallTools.printTimeAndThread("服务员开了面额500的发票");
            SmallTools.sleep(150);
            return "500元发票";
        });
        SmallTools.printTimeAndThread("小白在接电话，朋友约他一起开黑");
        SmallTools.printTimeAndThread(String.format("小白拿到了%s,开始回家", invoiced.join()));
    }

    public static void invoice1() {
        SmallTools.printTimeAndThread("小白吃完饭了 结账并要求开发票");
        CompletableFuture<String> invoiced = CompletableFuture.supplyAsync(() -> {
            SmallTools.printTimeAndThread("服务员收款500元");
            SmallTools.sleep(60);
            CompletableFuture<String> invoice = CompletableFuture.supplyAsync(() -> {
                SmallTools.printTimeAndThread("服务员开票500元");
                SmallTools.sleep(60);
                return "500元发票";
            });
            return invoice.join();
        });
        SmallTools.printTimeAndThread("小白在接电话，朋友约他一起开黑");
        SmallTools.printTimeAndThread(String.format("小白拿到了%s,开始回家", invoiced.join()));
    }

    public static void thenApply() {
        SmallTools.printTimeAndThread("小白吃完饭了 结账并要求开发票");
        CompletableFuture<String> invoice = CompletableFuture.supplyAsync(() -> {
            SmallTools.printTimeAndThread("服务员收费200元");
            SmallTools.sleep(200);
            return "200元";
        }).thenApplyAsync((money) -> {
            SmallTools.printTimeAndThread("服务员开具" + money + "发票");
            SmallTools.sleep(200);
            return "200元发票";
        });
        SmallTools.printTimeAndThread("小白在接电话，朋友约他一起开黑");
        SmallTools.printTimeAndThread(String.format("小白拿到了%s,开始回家", invoice.join()));

    }

    public static void main(String[] args) {
//        invoice1();
        thenApply();
    }
}
