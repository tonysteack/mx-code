/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.juc;

import lombok.ToString;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author tao.qu
 * @version 1.0
 **/
@ToString
public class DelayItem implements Delayed {
    private String name;
    private long time;

    public DelayItem(String name, long time) {
        this.name = name;
        this.time = time + System.currentTimeMillis();
    }
    @Override
    public long getDelay(TimeUnit unit) {
        long diff = time - System.currentTimeMillis();
        return unit.convert(diff, TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed o) {
        if (this.time < ((DelayItem)o).time) {
            return  -1;
        }
        if (this.time > ((DelayItem)o).time) {
            return 1;
        }
        return 0;
    }
}

class DelayItemTest {
    public static void main(String[] args) throws Exception {
        BlockingQueue<DelayItem> queue = new DelayQueue<>();
        queue.add(new DelayItem("A",1000*10));
        queue.add(new DelayItem("V",1000*7));
        queue.add(new DelayItem("M",1000*3));
        queue.add(new DelayItem("N",1000*13));

        System.out.println(queue.size());
        System.out.println(queue.element());//获取第一个元素，但是不从队列中取出
        System.out.println(queue.take());//从队列中取出第一个元素，按照元素的过期时间来排序
        System.out.println(queue.take());
        System.out.println(queue.take());
    }
}