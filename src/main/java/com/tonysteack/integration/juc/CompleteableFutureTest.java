/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.juc;

import com.tonysteack.integration.design.observe.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.util.StopWatch;

import java.util.concurrent.*;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class CompleteableFutureTest {

    public static void main(String[] args) throws Exception{
        StopWatch watch = new StopWatch();
        watch.start();
        UserInfoService userInfoService = new UserInfoService();
        MedalService medalService = new MedalService();
        userInfoService.getUserInfo(18);
        medalService.getMedalInfo(18);
        TimeUnit.MILLISECONDS.sleep(300);
        watch.stop();
        System.out.println("test1 costs = "+watch.getTotalTimeSeconds());
        System.out.println("**********************************************************************");
        StopWatch watch1 = new StopWatch();
        watch1.start();
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        FutureTask<User> task1 = new FutureTask<>(() -> userInfoService.getUserInfo(18));
        executorService.submit(task1);
        FutureTask<MedalInfo> task2 = new FutureTask<>(() -> medalService.getMedalInfo(18));
        executorService.submit(task2);
        TimeUnit.MILLISECONDS.sleep(300);
        task1.get();
        task2.get();
        watch1.stop();
        System.out.println("test2 costs = " + watch1.getTotalTimeSeconds());
        executorService.shutdown();
        System.out.println("**********************************************************************");
        StopWatch watch2 = new StopWatch();
        watch2.start();
        CompletableFuture<User> future1 = CompletableFuture.supplyAsync(() -> {
            try {
                return userInfoService.getUserInfo(18);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        });

        CompletableFuture<MedalInfo> future2 = CompletableFuture.supplyAsync(() -> {
            try {
                return medalService.getMedalInfo(18);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        });
        TimeUnit.MILLISECONDS.sleep(300);
        future1.get();
        future2.get();
        watch2.stop();
        System.out.println("test3 cost:" + watch2.getTotalTimeSeconds());
    }
}

class UserInfoService {
    public User getUserInfo(int id) throws Exception{
        TimeUnit.MILLISECONDS.sleep(300);
        return new User(id,"user" + id, "男", 18);
    }
}

class MedalService {
    public MedalInfo getMedalInfo(int id) throws Exception{
        TimeUnit.MILLISECONDS.sleep(600);
        return new MedalInfo(id, "守护者勋章");
    }
}

@Data
@AllArgsConstructor
class MedalInfo {
    int id;
    String name;
}