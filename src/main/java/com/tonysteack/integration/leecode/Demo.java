/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.leecode;

import java.util.Scanner;

/**
 * 回文数
 *
 * @author tao.qu
 * @version 1.0
 **/
public class Demo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputString;
        while ((inputString = scanner.nextLine()) != null) {
            if (inputString.equals("break")) {
                break;
            }
            System.out.println(romanToNumber(inputString));
        }
    }

    /**
     * 9
     * 测试回文数字
     *
     * @param str
     * @return
     */
    public static boolean isWantedNum(String str) {
        boolean isWantedNum = false;
        int length = str.length();
        if (length == 0 || "".equals(str)) {
            return isWantedNum;
        }
        try {
            if (length % 2 != 0) {
                String headStr = str.substring(0, length / 2);
                String endStr = str.substring(length / 2 + 1);
                if (new StringBuffer(headStr).reverse().toString().equals(endStr)) {
                    isWantedNum = true;
                }
            } else {
                String headStr = str.substring(0, length / 2);
                String endStr = str.substring(length / 2);
                if (new StringBuffer(headStr).reverse().toString().equals(endStr)) {
                    isWantedNum = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isWantedNum;
    }

    private static int romanToNumber(String romanStr) {
        int res = 0;
        romanStr = RomanNum.replace(romanStr);
        String[] split = romanStr.split("");
        for (int i = 0; i < split.length; i++) {
            res = RomanNum.which(split[i]) + res;
        }
        return res;
    }

    static class RomanNum {

        private static int which(String str) {
            switch (str) {
                case "I" : return 1;
                case "V" : return 5;
                case "X" : return 10;
                case "L" : return 50;
                case "C" : return 100;
                case "D" : return 500;
                case "M" : return 1000;
                case "a" : return 4;
                case "b" : return 9;
                case "c" : return 40;
                case "d" : return 90;
                case "e" : return 400;
                case "f" : return 900;
                default: return 0;
            }
        }

        private static String replace(String str) {
            if (str.contains("IV")) {
                str = str.replace("IV", "a");
            }
            if (str.contains("IX")) {
            str = str.replace("IX","b");
            }
            if (str.contains("XL")) {
                str = str.replace("XL","c");
            }
            if (str.contains("XC")) {
                str = str.replace("XC","d");
            }
            if (str.contains("IX")) {
                str = str.replace("IX","e");
            }
            if (str.contains("CD")) {
                str = str.replace("CD","f");
            }
            if (str.contains("CM")) {
                str = str.replace("CM","b");
            }
            return str;
        }
    }
}
