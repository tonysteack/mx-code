/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.reflect;

import java.io.File;
import java.util.*;

/**
 * @author tao.qu
 */
public class TestReflect {

    public List<String> getAllClass(){
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        classLoader.getResource("");

        return null;
    }

    public Map<String, String> getClasses(File f,Map<String, String> map){
        if (null==f || !f.exists()){
            return new HashMap<>();
        }
        if (f.isFile()){
            map.put(f.getName(),f.getName());
            return map;
        }
        for (File file : f.listFiles()) {
            getClasses(file,map);
        }
        return map;
    }
    public static void listAllFiles(File dir){
        if (null == dir || !dir.exists()){
            return;
        }
        if (dir.isFile()){
            System.out.println(dir.getName());
            return;
        }
        for (File file:dir.listFiles()) {
            listAllFiles(file);
        }
    }

    public List<String> getFiles(File f,List<String> list){
        if (!f.exists() || f == null){
            return Collections.emptyList();
        }
        if (f.isFile()){
            list.add(f.getName());
            return list;
        }
        for (File file : f.listFiles()) {
            getFiles(file,list);
        }
        return list;
    }

    public static void main(String[] args) {
        Map<String,String> map = new HashMap<>();

        TestReflect reflect = new TestReflect();
        List<String> list = new ArrayList<>();
//        List<String> files = reflect.getFiles(new File(path), list);
//        files.stream().forEach(System.out::println);

//        Map<String, String> map1 = reflect.getClasses(new File(path), map);
//        for (Map.Entry<String, String> entry : map1.entrySet()) {
//            System.out.println(entry.getKey());
//        }
    }
}
