/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class LockTest {
    private int num = 0;

    static class InnerClass {
        private final static LockTest obj = new LockTest();
        static Lock lock = new ReentrantLock(true);

        void increase() {
            lock.lock();
            try {
                System.out.println("obj=" + obj);
                int num = obj.getNum();
                num++;
                System.out.println("num=" + num);
                obj.setNum(num);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                new InnerClass().increase();
            }).start();
        }
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
