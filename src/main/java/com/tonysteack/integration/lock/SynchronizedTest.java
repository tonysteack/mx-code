/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.lock;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class SynchronizedTest {
    private int num = 1;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    static class InnerClass {
        private static final SynchronizedTest test = new SynchronizedTest();
        static {
            System.out.println("test:"+test);
        }
        private synchronized void increase() {
            int num = test.getNum();
            num++;
            test.setNum(num);
            System.out.println("currentNum:" + num);
            System.out.println("test:"+test);
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                System.out.println("name:" + Thread.currentThread().getName());
                new InnerClass().increase();
            }).start();
        }
    }
}
