/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.advice;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.concurrent.TimeUnit;

/**
 * @author tao.qu
 * @version 1.0
 **/
@Aspect
@Component
@Slf4j
public class ServiceAop {

    @Pointcut("execution(public * com.tonysteack.integration.service..*.*(..))*")
    private void service() {}

    @Before("service()")
    public void doBefore(JoinPoint point) throws InterruptedException {
        StopWatch watch = new StopWatch();
        watch.start();
        log.info("getDeclaringType {}", point.getSignature().getDeclaringType());
        log.info("getSourceLocation", point.getSourceLocation());
        log.info("getArgs", point.getArgs());
        log.info("getTarget", point.getTarget());
        log.info("getStaticPart", point.getStaticPart());
        log.info("getKind", point.getKind());
        log.info("getName", point.getSignature().getName());
        log.info("getDeclaringTypeName", point.getSignature().getDeclaringTypeName());
        log.info("toShortString", point.getSignature().toShortString());
        log.info("getModifiers", point.getSignature().getModifiers());
        ServletRequestAttributes servletRequest = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequest.getRequest();
        log.info("********************************start**************");
        log.info("request url:{},requestParams{}", request.getRequestURI(), JSON.toJSONString(request.getParameterMap()));
        HttpSession session = (HttpSession) servletRequest.resolveReference(RequestAttributes.REFERENCE_SESSION);
        log.info("sessionId:{}", session.getId());
        TimeUnit.MILLISECONDS.sleep(10);
        watch.stop();
        log.info("******do before cost times:{}", watch.getTotalTimeMillis());
    }

    @Around("service()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        StopWatch watch = new StopWatch();
        watch.start();
        Object[] args = point.getArgs();
        log.info("method name {}", point.getSignature().getName());
        for (Object arg : args) {
            log.info("args[]", arg);
        }
        Object result = point.proceed();
        log.info("打印出参：{}", JSON.toJSONString(result));
        watch.stop();
        log.info("around cost times{},result{}", watch.getTotalTimeMillis(),result);
        return result;
    }

    @After("service()")
    public void doAfter(JoinPoint point) {
        log.info("do after............");
        log.info(" do after getArgs", point.getArgs());
        log.info("do after getTarget", point.getTarget());
    }
}

