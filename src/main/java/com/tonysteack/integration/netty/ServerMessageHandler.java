/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.netty;

import com.tonysteack.integration.utils.DateUtils;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.util.HashMap;
import java.util.Map;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class ServerMessageHandler extends SimpleChannelInboundHandler<String> {
    private ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    private Map<String, Channel> all = new HashMap<>();

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, String s) throws Exception {
        Channel channel = channelHandlerContext.channel();
        if (s.contains("#")) {
            String id = s.split("#")[0];
            String body = s.split("#")[1];
            Channel idChannel = all.get(id);
            String key = idChannel.remoteAddress().toString().split(":")[1];
            idChannel.writeAndFlush(DateUtils.getCurDateTime() + "\n user" + key + "说了：" + body);
            return;
        }
        for (Channel ch : channels) {
            String addr = ch.remoteAddress().toString();
            if (channel != ch) {
                ch.writeAndFlush(DateUtils.getCurDateTime() + "\n [用户]" + addr + "说了：" + s);
            } else {
                ch.writeAndFlush(DateUtils.getCurDateTime() + "\n [自己]" + addr + "说了：" + s);
            }
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        String addr = ctx.channel().remoteAddress().toString();
        System.out.println(DateUtils.getCurDateTime() + "\n 【用户】" + addr + "上线");
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        String addr = ctx.channel().remoteAddress().toString();
        System.out.println(DateUtils.getCurDateTime() + "\n 【用户】" + addr + "下线");
        String key = ctx.channel().remoteAddress().toString().split(":")[1];
        all.remove(key);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println("connect error连接异常");
        ctx.close();
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) {
        Channel channel = ctx.channel();
        String addr = channel.remoteAddress().toString();
        channel.writeAndFlush(DateUtils.getCurDate()+"\n[用户]"+addr+"加入聊天室");
        channels.add(channel);
        String key = channel.remoteAddress().toString().split(":")[1];
        all.put(key,channel);
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        Channel channel = ctx.channel();
        String addr = channel.remoteAddress().toString();
        channel.writeAndFlush(DateUtils.getCurDateTime()+"\n [用户]"+addr+"离开聊天室");
        System.out.println("当前在线人数是："+channels.size());
        System.out.println("all："+all.size());
    }
}
