/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

/**
 * netty聊天室服务器端
 *
 * @author tao.qu
 * @version 1.0
 **/
public class NettyChatServer {
    private int port;

    public NettyChatServer(int port) {
        this.port = port;
    }

    public void init() {
        NioEventLoopGroup boss = new NioEventLoopGroup(1);
        NioEventLoopGroup work = new NioEventLoopGroup(2);
        try {
            ServerBootstrap boot = new ServerBootstrap();
            boot.group(boss, work);
            boot.channel(NioServerSocketChannel.class);
            boot.option(ChannelOption.SO_BACKLOG, 128);
            boot.childOption(ChannelOption.SO_KEEPALIVE, true);
            boot.childHandler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) {
                    ChannelPipeline pipeline = socketChannel.pipeline();
                    pipeline.addLast("encoder", new StringEncoder());
                    pipeline.addLast("decoder", new StringDecoder());
                    pipeline.addLast(new ServerMessageHandler());
                }
            });
            System.out.println("聊天室服务器开始启动...");
            ChannelFuture channelFuture = boot.bind(port).sync();
            channelFuture.addListener(future -> {
                if (future.isSuccess()) {
                    System.out.println("服务器正在启动...");
                }
                if (future.isDone()) {
                    System.out.println("服务器已经启动");
                }
            });
            channelFuture.channel().closeFuture().sync();
            channelFuture.addListener(future -> {
                if (future.isCancelled()) {
                    System.out.println("服务器正在关闭");
                }
                if (future.isCancellable()) {
                    System.out.println("服务器已经关闭。。。");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            boss.shutdownGracefully();
            work.shutdownGracefully();
        }
    }

    public static void main(String[] args) throws Exception {
        new NettyChatServer(9000).init();
    }
}
