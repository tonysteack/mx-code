/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 多线程下载，支持断点续传 todo
 * seek(n) 从文件开始跳过n个字节
 * skipBytes(n) 从当前位置跳过n个字节
 *
 * @author tao.qu
 * @version 1.0
 **/
@Slf4j
@Getter
@Setter
public class DownloadUtils {
    private static final String IMG_PREFIX = "BIAN_";
    private String originURL;
    private int size;
    private InputStream inputStream;
    private String fileName;
    private String suffix;

    public DownloadUtils(String originURL) {
        this.originURL = originURL;
        try {
            if (!"".equals(originURL)) {
                URL url = new URL(originURL);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(5000);
                connection.setRequestMethod("GET");
                this.size = connection.getContentLength();
                this.inputStream = connection.getInputStream();
                this.fileName = NameUtils.getBiAnImageThumbName(originURL);
                this.suffix = NameUtils.getSuffixByUrl(originURL);
            }
        } catch (Exception e) {
            log.error("DownLoadUtils constructor init error {}", e);
        }
    }

    /**
     * 单线程下载
     *
     * @throws Exception
     */
    public void downloadToPath(String outputPath) throws Exception {
        byte[] bytes = new byte[1024];
        RandomAccessFile out = new RandomAccessFile(outputPath, "rw");
        int len;
        try {
            while ((len = this.inputStream.read(bytes)) != -1) {
                out.write(bytes, 0, len);
            }
        } catch (Exception e) {
            log.info("download file error");
        } finally {
            out.close();
        }
    }

    public static void main(String[] args) throws Exception {
//        new DownloadUtils("")
    }


    /**
     * 拷贝文件
     *
     * @param sourcePath 原始路径
     * @param targetPath 目标路径
     * @throws Exception
     */
    @Deprecated
    public void testCopyFile(String sourcePath, String targetPath) throws Exception {
        File originFile = new File(sourcePath);
        long fileSize = originFile.length();
        log.info("file size is {}", fileSize);
        //每次读取1M数据
        byte[] bytes = new byte[1024 * 1024];
        RandomAccessFile rw = new RandomAccessFile(originFile, "r");
        RandomAccessFile out = new RandomAccessFile(targetPath, "rw");
        int len = 0;
        while ((len = rw.read(bytes)) != -1) {
            out.write(bytes, 0, len);
        }
        out.close();
        rw.close();
    }
}
