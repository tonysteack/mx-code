/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.utils;

import java.util.Date;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class SmallTools {

    public static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void printTimeAndThread(String message) {
        System.out.println(new Date() + " |" + Thread.currentThread().getId()+ " |" + Thread.currentThread().getName() + " |" + message);
    }
}
