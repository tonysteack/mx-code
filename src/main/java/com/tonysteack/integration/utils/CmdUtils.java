/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class CmdUtils {

    public static String executeCommand(String cmdStr) {
        BufferedReader br = null;
        try {
            Process p = Runtime.getRuntime().exec(cmdStr);
            br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally
        {
            if (br != null)
            {
                try {
                    br.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static void main(String[] args) {
        String path = "D:\\code\\test\\src\\main\\resources\\image.png";
        String result = "cmd /c tesseract " + path + " out2 " + " -l chi_sim";
        System.out.println(result);
//        String []cmds = {"/bin/sh", "/c", result};
        result = executeCommand(result);
        System.out.println(result);
    }
}
