/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.utils;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.tonysteack.integration.bean.DrugDiseaseModel;
import com.tonysteack.integration.bean.UniversalDrugDiseaseMapping;
import com.tonysteack.integration.mapper.UniversalDrugDiseaseMappingMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author tao.qu
 * @version 1.0
 **/
@Slf4j
public class ExcelListener extends AnalysisEventListener<DrugDiseaseModel> {

    private UniversalDrugDiseaseMappingMapper mappingMapper;

    private static Map<String, String> map = DiseasesMap.map;

    public ExcelListener(UniversalDrugDiseaseMappingMapper mappingMapper) {
        this.mappingMapper = mappingMapper;
    }
    private List<DrugDiseaseModel> datas = new ArrayList<>();

    @Override
    public void invoke(DrugDiseaseModel drugDiseaseModel, AnalysisContext analysisContext) {
        if (analysisContext.getCurrentRowNum() > 1) {
            datas.add(drugDiseaseModel);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        log.info("data.size={}", datas.size());
        datas.forEach(data -> {
            save(data);
        });
    }

    private void save(DrugDiseaseModel model) {
        UniversalDrugDiseaseMapping mapping = transModelToMapping(model);
        mappingMapper.insertSelective(mapping);
    }

    private UniversalDrugDiseaseMapping transModelToMapping(DrugDiseaseModel model) {
        UniversalDrugDiseaseMapping mapping = new UniversalDrugDiseaseMapping();
        mapping.setDrugId(Long.parseLong(model.getDrugId()));
        mapping.setDrugName(model.getDrugName());
        mapping.setDrugTag(model.getDrugTag());
        mapping.setKeyword(model.getKeyWord());
        mapping.setCommodityName(model.getCommodityName());
        mapping.setCommodityId(0L);
        List<String> list = new ArrayList<>();
        if (StringUtils.isNotBlank(model.getBaixueBing()) && map.containsKey(model.getBaixueBing())) {
            list.add(map.get(model.getBaixueBing()));
        }
        if (StringUtils.isNotBlank(model.getLungCancer()) && map.containsKey(model.getLungCancer())) {
            list.add(map.get(model.getLungCancer()));
        }
        if (StringUtils.isNotBlank(model.getRuxianAi()) && map.containsKey(model.getRuxianAi())) {
            list.add(map.get(model.getRuxianAi()));
        }
        if (StringUtils.isNotBlank(model.getLinbaAi()) && map.containsKey(model.getLinbaAi())) {
            list.add(map.get(model.getLinbaAi()));
        }
        if (StringUtils.isNotBlank(model.getGanAi()) && map.containsKey(model.getGanAi())) {
            list.add(map.get(model.getGanAi()));
        }
        if (StringUtils.isNotBlank(model.getJieChangeAi()) && map.containsKey(model.getJieChangeAi())) {
            list.add(map.get(model.getJieChangeAi()));
        }
        if (StringUtils.isNotBlank(model.getLuanChaoAi()) && map.containsKey(model.getLuanChaoAi())) {
            list.add(map.get(model.getLuanChaoAi()));
        }
        if (StringUtils.isNotBlank(model.getWeiAi()) && map.containsKey(model.getWeiAi())) {
            list.add(map.get(model.getWeiAi()));
        }
        if (StringUtils.isNotBlank(model.getQianliAi()) && map.containsKey(model.getQianliAi())) {
            list.add(map.get(model.getQianliAi()));
        }
        if (StringUtils.isNotBlank(model.getShiGuanAi()) && map.containsKey(model.getShiGuanAi())) {
            list.add(map.get(model.getShiGuanAi()));
        }
        if (StringUtils.isNotBlank(model.getGuliuAi()) && map.containsKey(model.getGuliuAi())) {
            list.add(map.get(model.getGuliuAi()));
        }
        if (StringUtils.isNotBlank(model.getGongJingAi()) && map.containsKey(model.getGongJingAi())) {
            list.add(map.get(model.getGongJingAi()));
        }
        if (list.size() > 0) {
            mapping.setOtherFlag(false);
        } else {
            mapping.setOtherFlag(true);
        }
        String[] array = list.toArray(new String[list.size()]);
        mapping.setDiseaseCodes(String.join(",", array));
        return mapping;
    }
}
