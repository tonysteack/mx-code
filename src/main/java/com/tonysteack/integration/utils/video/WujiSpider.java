package com.tonysteack.integration.utils.video;

import com.tonysteack.integration.bean.VideoEntity;
import com.tonysteack.integration.utils.HtmlUtils;
import com.tonysteack.integration.utils.SpiderUtils;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.Collections;
import java.util.List;

@Slf4j
public class WujiSpider extends SpiderUtils {
    private static final String demoUrl = "https://5ji.tv//search.html?wd=%E5%89%91%E7%8E%8B%E6%9C%9D&submit=";
    private static final String videotag = "span[class=pic-text1 text-right] b";
    private static final String clearTag = "span[class=pic-text text-right] b";
    private static final String introduce1 = "div[class=stui-pannel__bd] div:nth-child(14) p:nth-child(2)";
    private static final String searchResults = "body > div:nth-child(1) > div > div > div.stui-pannel__bd.clearfix > ul > li > div";
    private static final String videoNameTag = "div[class=stui-vodlist__detail]\").select(\"h4[class=title text-overflow] a";
    private static final String userListTag = "stui-vodlist clearfix";


    @Override
    public List<VideoEntity> searchVideo(String url) {
        Elements videoDivs = getSearchResultsDivs();
        videoDivs.forEach(div -> {
            VideoEntity entity = new VideoEntity();
            Elements aTag = div.select("a");
            String thumb = aTag.attr("data-original");
            String name = div.select(videoNameTag).text();
            String tag = div.select(videotag).text();
            String pageUrl = WujiHtmlUtils.url + aTag.attr("href");
            String cleardesc = aTag.select(clearTag).text();
            Document document = WujiHtmlUtils.getPage(pageUrl);
            String introduce = document.select(introduce1).text();
//            document

            entity.setThumb(thumb)
                    .setTag(tag)
                    .setPageUrl(pageUrl)
                    .setIntroduce(introduce)
                    .setClearDesc(cleardesc)
                    .setName(name);
            System.out.println(entity);
        });
        return Collections.emptyList();
    }

    private Elements getSearchResultsDivs() {
        Document document = HtmlUtils.getPage(demoUrl);
        try {
            assert document != null;
        } catch (Exception e) {
            log.error("网页格式捕获异常");
        }
        return document.select(searchResults);
    }

    private String getVideoIntroduce(String url){
        Document document = WujiHtmlUtils.getPage(url);
        return document.select(introduce1).text();
    }

    public static void main(String[] args) {
        /**
         * 获取查询结果首页
         */
//        Document document = WujiHtmlUtils.getPage(demoUrl);
//        System.out.println(document);
        /**
         * https://5ji.tv/kan/34035.html
         * 获取影视作品首页包括相关介绍、
         */
        Document document = WujiHtmlUtils.getPage("https://5ji.tv/kan/34035.html");
        System.out.println(document);
//        WujiSpider spider = new WujiSpider();
//        spider.searchVideo(demoUrl);
//        String url = "https://5ji.tv/kan/44181.html";
//        spider.getVideoIntroduce(url);
    }
}
