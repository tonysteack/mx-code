/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.utils;

import org.junit.Test;
import org.springframework.util.Assert;

import java.util.Objects;

/**
 * 根据网络资源URL截取得到图片名称
 *
 * @author tao.qu
 * @version 1.0
 **/
public class NameUtils {

    public static String getBiAnImageThumbName(String url) {
        Assert.notNull(url, "url can't be null(网络资源路径不能为空！)");
        return url.substring(url.lastIndexOf("/")+1, url.lastIndexOf("."));
    }

    @Test
    public void getBiAnImageThumbName() {
        String url = "https://pic.netbian.com/uploads/allimg/180826/113958-153525479855be.jpg";
        System.out.println(NameUtils.getBiAnImageThumbName(url));
    }

    public static String getSuffixByUrl(String url) {
        Objects.requireNonNull(url, "url不能为空!");
        return url.substring(url.lastIndexOf("."+1));
    }
}
