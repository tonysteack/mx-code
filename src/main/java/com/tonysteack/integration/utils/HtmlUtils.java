package com.tonysteack.integration.utils;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Slf4j
public abstract class HtmlUtils {
    /**
     * 获取视频地址页面
     *
     * @param url
     * @return
     */
    public static Document getPage(String url) {
        Document document;
        try {
            String userAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3947.100 Safari/537.36";
            document = Jsoup.connect(url).header("User-Agent", userAgent).timeout(50000000).get();
            return document;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 单线程下载
     *
     * @param url
     * @param name
     */
    public static void downLoadByUrl(String url, String name) {
        FileOutputStream writer;
        InputStream inputStream;
        int read = 0;
        try {
            URL u = new URL(url);
            byte[] store = new byte[1024];
            HttpURLConnection connection = (HttpURLConnection) u.openConnection();
            connection.setConnectTimeout(5*1000);
            connection.setRequestMethod("GET");
            connection.setRequestProperty(
                    "Accept",
                    "image/gif,image/jpeg,image/pjpeg,"
                            +"application/x-shockwave-flash,application/xaml+xml,"
                            +"application/vnd.ms-xpsdocument,application/x-ms-xbap,"
                            +"application/x-ms-application,application/vnd.ms-excel,"
                            +"application/vnd.ms-powerpoint,application/msword, */*"
            );
            connection.setRequestProperty("Accept-Language","zh-CN");
            connection.setRequestProperty("Charset","UTF-8");
            connection.setRequestProperty("User-agent", "	Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
            inputStream = connection.getInputStream();
            File file = new File(name);
            if (!file.exists()) {
                file.createNewFile();
            }
            writer = new FileOutputStream(file);
            while ((read = inputStream.read(store)) != -1) {
                writer.write(store, 0, read);
            }
            writer.close();
            inputStream.close();
            System.out.println(url + "下载完成");
        } catch (MalformedURLException e) {
            System.out.println(url + "连接超时");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
