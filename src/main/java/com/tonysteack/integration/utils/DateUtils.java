/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.utils;

import lombok.extern.slf4j.Slf4j;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;

/**
 * @author tao.qu
 * @version 1.0
 */
@Slf4j
public class DateUtils {
    private static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_FORMAT = "yyyyMMdd";
    private static final String DATETIME_FORMAT_WITH_MILLISECOND = "yyyy-MM-dd HH:mm:ss.SSS";

    /**
     * 将当前时间转换为年月日时分秒的字符串
     *
     * @return String
     */
    public static String getCurDateTime() {
        return dateToLocalDateTime(new Date()).format(DateTimeFormatter.ofPattern(DATETIME_FORMAT));
    }

    /**
     * 将当前日期转换为年月日的字符串
     *
     * @return String
     */
    public static String getCurDate() {
        return dateToLocalDateTime(new Date()).format(DateTimeFormatter.ofPattern(DATE_FORMAT));
    }

    /**
     * 获取本月第一天
     *
     * @return
     */
    public static LocalDate firstDayOfMonth() {
        return firstDayOfMonth(LocalDate.now());
    }

    /**
     * 获取给定日期的当月第一天
     *
     * @param date
     * @return
     */
    public static LocalDate firstDayOfMonth(LocalDate date) {
        return date.with(TemporalAdjusters.firstDayOfMonth());
    }

    /**
     * 判断指定日期是否为当月第一天
     *
     * @param date
     * @return boolean
     */
    public static boolean isFirstDayOfMonth(Date date) {
        return dateToLocalDate(date).equals(firstDayOfMonth(dateToLocalDate(date)));
    }

    /**
     * 获取本月第一天
     *
     * @return
     */
    public static LocalDate lastDayOfMonth() {
        return lastDayOfMonth(LocalDate.now());
    }

    /**
     * 获取给定日期的当月第一天
     *
     * @param date
     * @return
     */
    public static LocalDate lastDayOfMonth(LocalDate date) {
        return date.with(TemporalAdjusters.lastDayOfMonth());
    }

    /**
     * 判断指定日期是否为当月最后一天
     *
     * @param date
     * @return boolean
     */
    public static boolean isLastDayOfMonth(Date date) {
        return lastDayOfMonth(dateToLocalDate(date)).equals(dateToLocalDate(date));
    }

    /**
     * 字符串转为指定格式的日期
     *
     * @param dateStr
     * @param format
     * @return
     */
    public static Date dateStrToDateByFormat(String dateStr, String format) {
        return localDateTimeToDateWithTimeZone(LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(format)), null);
    }

    /**
     * 获取当前日期 + {days}的 日期
     *
     * @param days
     * @return
     */
    public static Date plusDays(int days) {
        return Date.from(ZonedDateTime.now().plusDays(days).toInstant());
    }

    /**
     * 给定时区将LocalDateTime转为Date
     * @param time
     * @param timeZone
     * @return
     */
    public static Date localDateTimeToDateWithTimeZone(LocalDateTime time, ZoneId timeZone) {
        if (timeZone == null) {
            timeZone = ZoneId.systemDefault();
        }
        return Date.from(time.atZone(timeZone).toInstant());
    }

    /**
     * 判断指定日期date和当前日期的时间差是否超过{days}天（精确到秒）
     *
     * @param date
     * @param days
     * @return
     */
    public static boolean calculateTimeGap(Date date, Integer days) {
        return !dateToDateTimeAndPlusDays(date, days).isBefore(LocalDateTime.now());
    }

    /**
     * 计算两个日期之间相差多少天
     * @param from
     * @param to
     * @return
     */
    public static long calculateDateGap(Date from, Date to) {
        LocalDateTime fromDate = dateToLocalDateTime(from);
        LocalDateTime toDate = dateToLocalDateTime(to);
        Duration duration = Duration.between(fromDate, toDate);
        return Math.abs(duration.toDays());
    }

    /**
     * 将localdate转为date
     */
    public static Date localDateToDateWithTimeZone(LocalDate date, ZoneId timeZone) {
        if (timeZone == null) {
            timeZone = ZoneId.systemDefault();
        }
        return Date.from(date.atStartOfDay(timeZone).toInstant());
    }

    public static LocalDateTime getBeginTimeOfSpecifyDay(Date date) {
        return dateToLocalDateTime(date).with(LocalTime.MIN);
    }

    /**
     * 获取指定日期前几天的的零时零分零秒
     *
     * @param date 指定日期
     * @param days 提前天数，可以为负，默认为0
     * @return Date
     */
    public static Date getBeginTimeStampOfSpecifyDay(Date date, int days) {
        if (date == null) {
            return null;
        }
        return localDateTimeToDateWithTimeZone(getBeginTimeOfSpecifyDay(date).plusDays(days), null);
    }

    /**
     * 获取指定日的最大时分秒
     *
     * @param date
     * @return
     */
    public static LocalDateTime getEndTimeOfSpecifyDay(Date date) {
        return dateToLocalDateTime(date).with(LocalTime.MAX);
    }

    /**
     * 获取指定日期+ {days}的最大时分秒 如  Sun Nov 14 23:59:59 CST 2021
     *
     * @param date 时间，默认当前日期
     * @param days 天数，默认为0
     * @return Date
     */
    public static Date getEndTimeStampOfSpecifyDay(Date date, int days) {
        if (date == null) {
            return null;
        }
        return localDateTimeToDateWithTimeZone(getEndTimeOfSpecifyDay(date).plusDays(days), null);
    }

    private static LocalDateTime dateToDateTimeAndPlusDays(Date date, int days) {
        return dateToLocalDateTime(date).plusDays(days);
    }

    private static LocalDate toLocalDateAndPlusDays(Date date, int days) {
        return dateToLocalDate(date).plusDays(days);
    }

    /**
     * 将date类型转为LocalDateTime类型
     *
     * @param date
     * @return
     */
    private static LocalDateTime dateToLocalDateTime(Date date) {
        return ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).toLocalDateTime();
    }

    /**
     * 将Date类型转为LocalDate类型
     *
     * @param date
     * @return
     */
    private static LocalDate dateToLocalDate(Date date) {
        return dateToLocalDateTime(date).toLocalDate();
    }

    /**
     * 将年月日时分秒毫秒的字符串解析为date类型
     *
     * @param dateStr
     * @return
     */
    public static Date dateTimeWithMillionStrToDate(String dateStr) {
        return dateStrToDateByFormat(dateStr, DATETIME_FORMAT_WITH_MILLISECOND);
    }

}
