package com.tonysteack.integration.utils;

import com.tonysteack.integration.bean.VideoEntity;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

@Slf4j
public abstract class SpiderUtils {

    /**
     * 通过,3u8地址下载ts分短视频文件
     *
     * @param url
     * @param threadNum
     * @return
     */
    public boolean downloadVideo(String url, int threadNum, String targetPath) {
        CountDownLatch count = new CountDownLatch(threadNum);
        List<String> tsUrlList = getTsUrlList(url);
        List<List<String>> lists = averageAssign(tsUrlList, threadNum);
        File file = new File(targetPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        try {
            for (int d = 0; d < threadNum; d++) {
                final int i = d;
                new Thread(() -> {
                    List<String> strings = lists.get(i);
                    for (String string : strings) {
                        String name = getFileName(string);
                        HtmlUtils.downLoadByUrl(string, targetPath + File.separator + name);
                    }
                    count.countDown();
                }).start();
            }
            count.await();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public List<String> getTsUrlList(String url) {
        String headUrl = getHeadUrl(url);
        List<String> list = getAllTrueUrl(url);
        List<String> collect = list.stream().map(l -> headUrl + l).collect(Collectors.toList());
        return collect;
    }

    private String getFileName(String str) {
        return str.substring(str.lastIndexOf("/") + 1, str.length());
    }

    public String getHeadUrl(String m3u8Url) {
        return m3u8Url.substring(0, m3u8Url.indexOf("com") + 3);
    }

    /**
     * 根据第一层获取的真实的M3U8地址获取所有的子视频片段地址
     *
     * @param trueUrl
     * @return
     */
    public List<String> getAllTrueUrl(String trueUrl) {
        List<String> result = new ArrayList<>();
        BufferedReader reader;
        try {
            URL url = new URL(trueUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("/")) {
                    result.add(line);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static <T> List<List<T>> averageAssign(List<T> source, int n) {
        List<List<T>> result = new ArrayList<List<T>>();
        int remaider = source.size() % n; //(先计算出余数)
        int number = source.size() / n; //然后是商
        int offset = 0;//偏移量
        for (int i = 0; i < n; i++) {
            List<T> value = null;
            if (remaider > 0) {
                value = source.subList(i * number + offset, (i + 1) * number + offset + 1);
                remaider--;
                offset++;
            } else {
                value = source.subList(i * number + offset, (i + 1) * number + offset);
            }
            result.add(value);
        }
        return result;
    }

    public abstract List<VideoEntity> searchVideo(String url);
}
