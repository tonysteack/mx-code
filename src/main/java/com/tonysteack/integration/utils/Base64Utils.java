/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.utils;

import cn.hutool.core.codec.Base64;

import java.io.*;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class Base64Utils {

    public static String encodeImage(String imagePath) throws IOException {
        InputStream in;
        byte[] data;
        String encode = null;
        try {
            in = new FileInputStream(imagePath);
            data = new byte[in.available()];
            int read = in.read(data);
            encode = Base64.encode(data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return encode;
    }

    public static boolean generateImage(String imageData, String imagePath)throws Exception{
        if (imageData == null){
            return false;
        }
        OutputStream out = null;
        try {
            out = new FileOutputStream(imagePath);
            byte[] imageByte = Base64.decode(imageData);
            for (int i = 0; i < imageByte.length; i++) {
                if (imageByte[i]<0){
                    imageByte[i] += 256;
                }
            }
            out.write(imageByte);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            out.flush();
            out.close();
            return true;
        }
    }

    public static void main(String[] args) throws Exception {
        String path = "D:\\微信图片_20210917174205.png";
        String outPath = "D://test.png";
        String imageData = encodeImage(path);
        System.out.println(generateImage(imageData,outPath));
    }

}
