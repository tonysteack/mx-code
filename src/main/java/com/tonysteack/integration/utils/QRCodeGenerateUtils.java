/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.nio.file.FileSystems;
import java.nio.file.Path;

/**
 * 生成二维码
 *
 * @author tao.qu
 * @version 1.0
 **/
public class QRCodeGenerateUtils {
    /**
     * 生成二维码并写入到指定路径
     * @param text 文本信息
     * @param width 生成图片的宽度
     * @param height 生成图片的高度
     * @param filePath 路径
     * @throws Exception
     */
    public static void generateQRCodeImage(String text, int width, int height, String filePath) throws Exception {
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix matrix = writer.encode(text, BarcodeFormat.QR_CODE, width, height);
        Path path = FileSystems.getDefault().getPath(filePath);
        MatrixToImageWriter.writeToPath(matrix, "PNG",path);
    }

    public static void main(String[] args) throws Exception {
        generateQRCodeImage("wangna is a pig", 350, 350, "D:/test1.png");
    }
}
