/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * @author tao.qu
 */
public class PathUtils {
    private static String CLASS_PREFIX = File.separator + "class" + File.separator;
    private static String CLASS_SUFFIX = ".class";


    public static String getProjectRootPath() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        URL resource = classLoader.getResource("");
        String path = resource.getPath();
        if (path.startsWith("/")) {
            path = path.substring(path.indexOf("/") + 1);
        }
        return path;
    }

    public static List<String> getFiles(String path, List<String> list) {
        File file = new File(path);
        if (!file.exists()) {
            return list;
        }
        if (file.isFile()) {
            list.add(file.getName());
            return list;
        }
        for (File listFile : file.listFiles()) {
            String absolutePath = listFile.getAbsolutePath();
//            System.out.println(absolutePath);
            getFiles(absolutePath, list);
        }
        return list;
    }

    public static List<String> getAllClassNameByFile(File file, boolean scanSonPackage) {
        List<String> list = new ArrayList<>();
        if (!file.exists()) {
            return list;
        }
        if (file.isFile()) {
            String path = file.getPath();
            if (path.endsWith(".class")) {
                path = path.replace(".class", "");
                String clazzName = path.substring(path.indexOf(CLASS_PREFIX) + CLASS_PREFIX.length())
                        .replace(File.separator, ".");
                if (clazzName.indexOf("$") == -1) {
                    list.add(clazzName);
                }
            }
            return list;
        } else {
            File[] files = file.listFiles();
            if (files.length>0 && files!=null){
                for (File file1 : files) {
                    if (scanSonPackage){
                        list.addAll(getAllClassNameByFile(file1,scanSonPackage));
                    }else {
                        if (file1.isFile()){
                            String path = file1.getPath();
                            if (path.endsWith(CLASS_SUFFIX)){
                                path = path.replace(CLASS_SUFFIX,"");
                                String className = path.substring(path.indexOf(CLASS_PREFIX)+CLASS_PREFIX.length()).replace(File.separator,".");
                                if (className.contains("$")){
                                    list.add(className);
                                }
                            }
                        }
                    }

                }
            }
        }
        return list;
    }

    public static List<String> getAllClassFileByPackageName(String packageName, boolean scanSonPackage) {
        List<String> list = new ArrayList<>();
        String suffixPath = packageName.replace("\\.", "/");
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try {
            Enumeration<URL> resources = loader.getResources(suffixPath);
            while (resources.hasMoreElements()) {
                URL url = resources.nextElement();
                System.out.println("url=" + url);
                if (url != null) {
                    if (url.getProtocol().equals("file")) {
                        String path = url.getPath();
                        System.out.println(path);
                        list.addAll(getAllClassNameByFile(new File(path),scanSonPackage));
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void main(String[] args) {
        String s = getProjectRootPath();
        System.out.println(s);
//        List<String> list = new ArrayList<>();
//        for (String file : getFiles(s, list)) {
//            System.out.println(file);
//        }
        List<String> list = getAllClassFileByPackageName("com.tonysteack.integration", true);
        list.forEach(System.out::println);
    }
}
