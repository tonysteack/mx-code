/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class DiseasesMap {
    public static Map<String, String> map = new ConcurrentHashMap<>();
    static {
        map.put("肺癌", "C34");
        map.put("乳腺癌", "C50");
        map.put("肝癌", "C22");
        map.put("淋巴瘤", "C81");
        map.put("白血病", "C91");
        map.put("结直肠癌", "C18");
        map.put("卵巢癌", "C56");
        map.put("胃癌", "C16");
        map.put("前列腺癌", "C61");
        map.put("食管癌", "C15");
        map.put("多发性骨髓瘤", "C90");
        map.put("宫颈癌", "C53");

    }
}
