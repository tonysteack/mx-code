/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author tao.qu
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ToString(callSuper = true)
public class ActorEntity extends BaseEntity{
    private Integer age;
    /**
     * true 男
     * false 女
     */
    private boolean gender;
    private String address;
    /**
     * 演出作品
     */
    private List<String> shows;
}
