package com.tonysteack.integration.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author tao.qu
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ToString(callSuper = true)
public class VideoEntity extends BaseEntity{
    private String thumb;
    private String videoUrl;
    /**
     * 分类
     */
    private String tag;

    private String pageUrl;
    /**
     * 剧情介绍
     */
    private String introduce;
    /**
     * 清晰度标签
     */
    private String clearDesc;

    /**
     * 演员列表
     */
    private List<ActorEntity> persons;

}
