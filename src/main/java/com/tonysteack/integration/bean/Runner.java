///**
// * www.meditrusthealth.com Copyright © MediTrust Health 2017
// */
//package com.tonysteack.integration.bean;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.flink.configuration.Configuration;
//import org.apache.flink.streaming.api.datastream.DataStreamSource;
//import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
//import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
//import org.apache.flink.streaming.api.functions.source.SourceFunction;
//import org.apache.kafka.common.config.Config;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.AnnotationConfigApplicationContext;
//import org.springframework.stereotype.Component;
//
//import java.util.concurrent.TimeUnit;
//
//@Component
//public class Runner implements CommandLineRunner {
//
//    @Override
//    public void run(String... args) throws Exception {
//        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
//        DataStreamSource<String> stream = environment.addSource(new SourceFunction<String>() {
//            @Override
//            public void run(SourceContext<String> sourceContext) throws Exception {
//                long c = 0;
//                while (true) {
//                    sourceContext.collect("test" + c++);
//                    TimeUnit.MILLISECONDS.sleep(3000);
//                }
//            }
//
//            @Override
//            public void cancel() {
//            }
//        });
//        stream.addSink(new MySink());
//        environment.execute("test job11");
//    }
//
//    @Slf4j
//    static class MySink extends RichSinkFunction<String> {
//        private AnnotationConfigApplicationContext context;
//        public MySink(){
//            System.out.println("mySink init");
//        }
//
//        @Override
//        public void open(Configuration parameters) throws Exception {
//            super.open(parameters);
//            this.context = new AnnotationConfigApplicationContext(Config.class);
//        }
//
//        @Override
//        public void invoke(String value, Context context) throws Exception {
//            super.invoke(value, context);
//            log.info("调用invoke"+value);
//        }
//
//        @Override
//        public void close() throws Exception {
//            super.close();
//            log.info("mySink close");
//        }
//    }
//}
