package com.tonysteack.integration.bean;

import lombok.Data;

import java.util.Date;

/**
 * @author tao.qu
 */
@Data
public class BaseEntity {
    private Long id;
    private String name;
    private Date createTime;
    private Date updateTime;
}
