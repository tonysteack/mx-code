/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.bean;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author tao.qu
 * @version 1.0
 **/
@Data
public class DrugDiseaseModel {
    @ExcelProperty(value="药品ID",index=1)
    private String drugId;
    @ExcelProperty(value="药品ID",index=2)
    private String drugName;
    @ExcelProperty(value="药品ID",index=3)
    private String commodityName;
    @ExcelProperty(value="药品ID",index=4)
    private String keyWord;
    @ExcelProperty(value="药品ID",index=5)
    private String drugTag;
    @ExcelProperty(value="药品ID",index=6)
    private String lungCancer;
    @ExcelProperty(value="药品ID",index=7)
    private String linbaAi;
    @ExcelProperty(value="药品ID",index=8)
    private String ruxianAi;
    @ExcelProperty(value="药品ID",index=9)
    private String ganAi;
    @ExcelProperty(value="药品ID",index=10)
    private String baixueBing;
    @ExcelProperty(value="药品ID",index=11)
    private String jieChangeAi;
    @ExcelProperty(value="药品ID",index=12)
    private String guliuAi;
    @ExcelProperty(value="药品ID",index=13)
    private String weiAi;
    @ExcelProperty(value="药品ID",index=14)
    private String luanChaoAi;
    @ExcelProperty(value="药品ID",index=15)
    private String shiGuanAi;
    @ExcelProperty(value="药品ID",index=16)
    private String qianliAi;
    @ExcelProperty(value="药品ID",index=17)
    private String gongJingAi;
}
