/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.bean;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * @author tao.qu
 * @version 1.0
 **/
@Data
@ToString
public class UniversalDrugDiseaseMapping {
    private Long id;
    private Long drugId;
    private String drugName;
    private Long commodityId;
    private String commodityName;
    private String drugTag;
    private String keyword;
    private String diseaseCodes;
    private String drugCommonName;
    /**
     * false 非其他  true 其他
     */
    private boolean otherFlag;
    private Date createTime;
    private Date updateTime;
    private String creator;
    private String modifier;
    private boolean delFlag;
}
