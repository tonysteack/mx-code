/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.structure;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class LinkNode {
    private LinkNode next;
    private Object data;
    private int size = 1;

    public LinkNode(Object data) {
        this.data = data;
    }

    public LinkNode getNext() {
        return next;
    }

    public void setNext(LinkNode next) {
        this.next = next;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int size() {
        LinkNode head = this;
        while (head.getNext() != null) {
            size++;
            head = head.getNext();
        }
        return size;
    }

    public LinkNode(LinkNode next, Object data) {
        this.next = next;
        this.data = data;
    }

    public List<Object> getLinkNodeData() {
        List<Object> results = new ArrayList<>();
        LinkNode head = this;
        do {
            results.add(head.getData());
            head = head.getNext();
        }while (head.getNext() != null);
        return results;
    }

    public static void main(String[] args) {
        LinkNode head = new LinkNode("node1");
        System.out.println(head.size());
        LinkNode second = new LinkNode("node2");
        head.setNext(second);
        second.setNext(new LinkNode("node3"));
        System.out.println(head.size());
        List<Object> objectList = head.getLinkNodeData();
        System.out.println("objectList.size():" + objectList.size());
        objectList.forEach(System.out::println);
    }

}
