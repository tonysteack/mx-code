/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.structure;

import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.function.Consumer;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class TreeNode<T> implements Iterable<TreeNode<T>> {
    private T data;
    private TreeNode<T> parent;
    private List<TreeNode<T>> child;
    private List<TreeNode<T>> allElements;

    public TreeNode(T data) {
        this.data = data;
        this.child = new ArrayList<>();
        this.allElements = new ArrayList<>();
        this.allElements.add(this);
    }

    public boolean isRoot() {
        return this.getParent() == null;
    }

    public boolean isLeaf() {
        return CollectionUtils.isEmpty(child);
    }

    public TreeNode<T> addChild(T child) {
        TreeNode<T> childNode = new TreeNode<>(child);
        childNode.setParent(this);
        allElements.add(childNode);
        this.child.add(childNode);
        return childNode;
    }

    public int getLevel() {
        int level = 1;
        if (this.isRoot()) {
            return level;
        }else {
            return parent.getLevel() + 1 ;
        }
    }

    public void registerChildForSearch(TreeNode<T> node) {
        allElements.add(node);
        if (parent != null) {
            parent.registerChildForSearch(node);
        }
    }

    public TreeNode<T> searchNode(Comparable<T> comparable){
        for (TreeNode<T> node : this.allElements) {
            T data = node.getData();
            if (comparable.compareTo(data) == 0) {
                return node;
            }
        }
        return null;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public TreeNode<T> getParent() {
        return parent;
    }

    public void setParent(TreeNode<T> parent) {
        this.parent = parent;
    }

    public List<TreeNode<T>> getChild() {
        return child;
    }

    public void setChild(List<TreeNode<T>> child) {
        this.child = child;
    }

    public List<TreeNode<T>> getAllElements() {
        return allElements;
    }

    public void setAllElements(List<TreeNode<T>> allElements) {
        this.allElements = allElements;
    }

    @Override
    public void forEach(Consumer<? super TreeNode<T>> action) {
        Iterable.super.forEach(action);
    }

    @Override
    public Spliterator<TreeNode<T>> spliterator() {
        return Iterable.super.spliterator();
    }

    @Override
    public Iterator<TreeNode<T>> iterator() {
        return new TreeNodeIterator<>(this);
    }

    static class TreeNodeIterator<T> implements Iterator<TreeNode<T>> {
        enum ProcessStages {
            ProcessParent, ProcessChildCurNode, ProcessChildSubNode
        }

        private ProcessStages doNext;

        private TreeNode<T> next;

        private Iterator<TreeNode<T>> childrenCurNodeIter;

        private Iterator<TreeNode<T>> childrenSubNodeIter;

        private TreeNode<T> treeNode;

        public TreeNodeIterator(TreeNode<T> treeNode) {
            this.treeNode = treeNode;
            this.doNext = ProcessStages.ProcessParent;
            this.childrenCurNodeIter = treeNode.child.iterator();
        }

        @Override
        public boolean hasNext() {

            if (this.doNext == ProcessStages.ProcessParent) {
                this.next = this.treeNode;
                this.doNext = ProcessStages.ProcessChildCurNode;
                return true;
            }

            if (this.doNext == ProcessStages.ProcessChildCurNode) {
                if (childrenCurNodeIter.hasNext()) {
                    TreeNode<T> childDirect = childrenCurNodeIter.next();
                    childrenSubNodeIter = childDirect.iterator();
                    this.doNext = ProcessStages.ProcessChildSubNode;
                    return hasNext();
                } else {
                    this.doNext = null;
                    return false;
                }
            }

            if (this.doNext == ProcessStages.ProcessChildSubNode) {
                if (childrenSubNodeIter.hasNext()) {
                    this.next = childrenSubNodeIter.next();
                    return true;
                } else {
                    this.next = null;
                    this.doNext = ProcessStages.ProcessChildCurNode;
                    return hasNext();
                }
            }

            return false;
        }

        @Override
        public TreeNode<T> next() {
            return this.next;
        }

        /**
         * 目前不支持删除节点
         */
        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
