/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.thread;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class CallableTest {
    public static void main(String[] args) throws Exception {
        CallableA ca = new CallableA("Mrs.Lee");
        FutureTask task = new FutureTask<>(ca);
        new Thread(task).start();
        TimeUnit.SECONDS.sleep(3);
        System.out.println(task.get());
    }
}

@Data
@AllArgsConstructor
class CallableA implements Callable<String> {
    private String msg;

    @Override
    public String call() throws Exception {
        return msg + "hello world!";
    }
}
