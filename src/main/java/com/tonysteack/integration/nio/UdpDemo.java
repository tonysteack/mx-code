/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.nio;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * @author tao.qu
 */
public class UdpDemo {
    public static class UdpServer{
        public static void main(String[] args) throws Exception {
            DatagramSocket socket = new DatagramSocket();
            String msg = "hello 666";

            DatagramPacket packet = new DatagramPacket(msg.getBytes(), msg.getBytes().length, InetAddress.getByName("127.0.0.1"), 10000);
            socket.send(packet);
            socket.close();
        }
    }

    public static class UdpClient{
        public static void main(String[] args) throws Exception{
            DatagramSocket socket = new DatagramSocket(10000);
            while (true){
                byte[] buf = new byte[1024];
                DatagramPacket packet = new DatagramPacket(buf, 0, buf.length);
                socket.receive(packet);
                String ip = packet.getAddress().getHostAddress();
                System.out.println(ip);
                String str = new String(packet.getData(), 0, packet.getLength());
                System.out.println("ip=" + ip + ",data=" + str);
            }
        }
    }
}

