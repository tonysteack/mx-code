/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * @author tao.qu
 */
public class NioServer {
    private ByteBuffer sendBuffer = ByteBuffer.allocate(1024);

    private ByteBuffer receiveBuffer = ByteBuffer.allocate(1024);
    private Selector selector;

    public NioServer(int port)throws Exception{
        ServerSocketChannel ssc = ServerSocketChannel.open();
        ssc.configureBlocking(false);
        ServerSocket socket = ssc.socket();
        socket.bind(new InetSocketAddress(port));
        selector = Selector.open();
        ssc.register(selector, SelectionKey.OP_ACCEPT);
        System.out.println("server start!");
    }

    private void lister()throws Exception{
        while (true){
            selector.select();
            Set<SelectionKey> keys = selector.selectedKeys();
            Iterator<SelectionKey> keyIterator = keys.iterator();
            while (keyIterator.hasNext()){
                SelectionKey key = keyIterator.next();
                keyIterator.remove();
                try {
                    handleKey(key);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void handleKey(SelectionKey key) throws IOException {
        ServerSocketChannel server;
        SocketChannel client;
        String receiveText;
        String sendText;
        int count;
        try {
            if (key.isAcceptable()){
                server = (ServerSocketChannel) key.channel();
                client = server.accept();
                client.configureBlocking(false);
                client.register(selector,SelectionKey.OP_READ);
            }else if(key.isReadable()){
                client = (SocketChannel) key.channel();
                receiveBuffer.clear();
                count = client.read(receiveBuffer);
                if (count>0){
                    receiveText = new String(receiveBuffer.array(),0,count);
                    System.out.println("服务器端接收客户端数据:" + receiveText);
                    client.register(selector,SelectionKey.OP_WRITE);
                }
            }else if(key.isWritable()){
                sendBuffer.clear();
                client = (SocketChannel) key.channel();
                sendText = "message from server";
                sendBuffer.put(sendText.getBytes());
                sendBuffer.flip();
                int write = client.write(sendBuffer);
                System.out.println("服务端向客户端发送数据:" + sendText);
                client.register(selector,SelectionKey.OP_READ);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        int port = 8088;
        NioServer server = new NioServer(port);
        server.lister();
    }
}
