/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Set;

/**
 * @author tao.qu
 */

public class ChatServer {
    public static void main(String[] args) {
        try {
            ServerSocketChannel open = ServerSocketChannel.open();
            open.bind(new InetSocketAddress("127.0.0.1",8001));
            open.configureBlocking(false);

            Selector selector = Selector.open();
            open.register(selector, SelectionKey.OP_ACCEPT);

            ByteBuffer readBuffer = ByteBuffer.allocate(1024);
            ByteBuffer writeBuffer = ByteBuffer.allocate(1024);
            writeBuffer.put("received".getBytes(StandardCharsets.UTF_8));
            writeBuffer.flip();

            while (true){
                int ready = selector.select();
                Set<SelectionKey> selectionKeys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = selectionKeys.iterator();
                while (iterator.hasNext()){
                    SelectionKey next = iterator.next();
                    iterator.remove();
                    if (next.isAcceptable()){
                        SocketChannel accept = open.accept();
                        accept.configureBlocking(false);
                        accept.register(selector,SelectionKey.OP_READ);
                    }else if (next.isReadable()){
                        SocketChannel socketChannel = (SocketChannel) next.channel();
                        readBuffer.clear();
                        socketChannel.read(readBuffer);
                        readBuffer.flip();
                        System.out.println("received:" + new String(readBuffer.array()));
                        next.interestOps(SelectionKey.OP_WRITE);
                    }else if (next.isWritable()){
                        writeBuffer.rewind();
                        SocketChannel socketChannel = (SocketChannel) next.channel();
                        socketChannel.write(writeBuffer);
                        next.interestOps(SelectionKey.OP_READ);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
