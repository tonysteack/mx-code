/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.nio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

/**
 * @author tao.qu
 */
public class ChatClient {
    public static void main(String[] args) {

        try {
            SocketChannel ssc = SocketChannel.open();
            ssc.connect(new InetSocketAddress("127.0.0.1", 8001));
            ByteBuffer readBuffer = ByteBuffer.allocate(32);
            ByteBuffer writeBuffer = ByteBuffer.allocate(32);

            writeBuffer.put("str".getBytes());
            writeBuffer.flip();
            while (true){
                writeBuffer.rewind();
                ssc.write(writeBuffer);
                readBuffer.clear();
                ssc.read(readBuffer);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static class test1 {
        public static void main(String[] args) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            String str;
            try {
                while ((str = bufferedReader.readLine()) != null) {
                    System.out.println(str.getBytes(StandardCharsets.UTF_8));
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
