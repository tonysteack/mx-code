/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.nio;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

/**
 * @author tao.qu
 */
public class TestBuffer {
    private final String BUFFER_MODE = "rw";

    public static void main(String[] args) throws IOException {
//        readBuffer();
        String pathFrom = "test.txt";
        String pathTo = "1.txt";
        TestBuffer testBuffer = new TestBuffer();
//        testBuffer.testTransform(pathFrom,pathTo);
//        testBuffer.readChannel(pathFrom);
        testBuffer.writeChannel(pathTo);
    }

    public void writeChannel(String path) throws IOException {
        FileChannel outChannel = new RandomAccessFile(path, BUFFER_MODE).getChannel();
        String data = "new String file"+System.currentTimeMillis();
        ByteBuffer buffer = ByteBuffer.allocate(32);
        buffer.clear();
        buffer.put(data.getBytes(StandardCharsets.UTF_8));
        buffer.flip();
        while (buffer.hasRemaining()){
            outChannel.write(buffer);
        }
    }

    public void readChannel(String path) {
        try {
            FileChannel channel = new RandomAccessFile(path, BUFFER_MODE).getChannel();
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
            System.out.println(channel.read(byteBuffer));
            byteBuffer.flip();
            while (byteBuffer.hasRemaining()) {
                System.out.println((char) byteBuffer.get());
            }
            byteBuffer.clear();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * @throws
     * @description:数据从pathFrom复制到pathTo
     * @param: [pathFrom, pathTo]
     * @return: void
     * @author tao.qu
     * @date: 2021/8/18 17:20
     */
    public void testTransform(String pathFrom, String pathTo) {
        try {
            RandomAccessFile from = new RandomAccessFile(pathFrom, BUFFER_MODE);
            FileChannel fromChannel = from.getChannel();

            RandomAccessFile to = new RandomAccessFile(pathTo, BUFFER_MODE);
            FileChannel toChannel = to.getChannel();
            long position = 0;
            long size = fromChannel.size();

            toChannel.transferFrom(fromChannel, position, size);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 从文件中读取数据
     */
    private static void readBuffer() {
        try {
            RandomAccessFile rw = new RandomAccessFile("test.txt", "rw");
            FileChannel fileChannel = rw.getChannel();
            ByteBuffer buffer = ByteBuffer.allocate(48);
            FileChannel outChannel = new FileOutputStream("2.txt").getChannel();
            while (true) {
                buffer.clear();
                int read = fileChannel.read(buffer);
                System.out.println("read=" + read);
                while (buffer.hasRemaining()) {
                    byte b = buffer.get();
                    Byte aByte = new Byte(b);
                    System.out.println((char) b);
                    if (aByte == null || aByte == 32) {
//                        buffer.flip();
//                        outChannel.write(buffer);
                        break;
                    }
                }
                if (read == -1) {
                    break;
                }
                buffer.flip();
                outChannel.write(buffer);
            }
//            while (read != -1){
//            System.out.println(read);
//                buffer.flip();
//                while(buffer.hasRemaining()){
//                    System.out.println((char) buffer.get());
//                }
//                buffer.clear();
//                read = fileChannel.read(buffer);
//            }
            rw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
