/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.nio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * @author tao.qu
 */
public class NioClient {
    private static final ByteBuffer sendBuffer = ByteBuffer.allocate(1024);
    private static final ByteBuffer receiveBuffer = ByteBuffer.allocate(1024);

    public static void main(String[] args) throws IOException {
        SocketChannel open = SocketChannel.open();
        open.configureBlocking(false);
        Selector selector = Selector.open();
        open.register(selector, SelectionKey.OP_CONNECT);
        open.connect(new InetSocketAddress("127.0.0.1",8088));
        Set<SelectionKey> selectionKeys;
        Iterator<SelectionKey> iterator;
        SelectionKey selectionKey;
        SocketChannel client;
        String receiveText;
        String sendText;
        int count;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true){
            selector.select();
            selectionKeys = selector.selectedKeys();
            iterator = selectionKeys.iterator();
            while (iterator.hasNext()){
                selectionKey = iterator.next();
                if (selectionKey.isConnectable()){
                    System.out.println("client connect");
                    client = (SocketChannel) selectionKey.channel();
                    if (client.isConnectionPending()){
                        client.finishConnect();
                        System.out.println("完成连接");
                        sendBuffer.clear();
                        sendBuffer.put("完成连接".getBytes());
                        sendBuffer.flip();
                        client.write(sendBuffer);
                    }
                    client.register(selector,SelectionKey.OP_READ);
                }else if(selectionKey.isReadable()){
                    client = (SocketChannel) selectionKey.channel();
                    receiveBuffer.clear();
                    count = client.read(receiveBuffer);
                    if (count>0){
                        receiveText = new String(receiveBuffer.array(),0,count);
                        System.out.println("客户端收到了" + receiveText);
                        client.register(selector,SelectionKey.OP_WRITE);
                    }
                }else if (selectionKey.isWritable()){
                    client = (SocketChannel) selectionKey.channel();
                    sendBuffer.clear();
                    sendText = reader.readLine();
                    if (sendText.equals("退出")){
                       return;
                    }
                    sendBuffer.put(sendText.getBytes());
                    sendBuffer.flip();
                    client.write(sendBuffer);
                    System.out.println("向服务端发送数据:"+sendText);
                    client.register(selector,SelectionKey.OP_READ);
                }
            }
        selectionKeys.clear();
        }
    }
}
