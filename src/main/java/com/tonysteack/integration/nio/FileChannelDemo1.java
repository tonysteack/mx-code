/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.nio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FileChannelDemo1 {
    public static void main(String[] args) throws Exception {
//        readFromFile();
//        readFromStr();
//        readFromInput();
//        transFromFileChannel();
        transferToFileChannel();
    }

    private static void transferToFileChannel() throws IOException {
        RandomAccessFile inFile = new RandomAccessFile("D:\\data\\bb.txt", "rw");
        RandomAccessFile outFile = new RandomAccessFile("D:\\data\\aa.txt", "rw");
        FileChannel inFileChannel = inFile.getChannel();
        FileChannel outFileChannel = outFile.getChannel();

        ByteBuffer buffer = ByteBuffer.allocate(2048);
        long position = 0;
        long size = outFileChannel.size();

        outFileChannel.transferTo(0,size,inFileChannel);

        outFile.close();
        inFile.close();
    }

    private static void transFromFileChannel() throws IOException {
        RandomAccessFile inFile = new RandomAccessFile("D:\\data\\bb.txt", "rw");
        RandomAccessFile outFile = new RandomAccessFile("D:\\data\\aa.txt", "rw");
        FileChannel inFileChannel = inFile.getChannel();
        FileChannel outFileChannel = outFile.getChannel();

        ByteBuffer buffer = ByteBuffer.allocate(2048);
        long position = 0;
        long size = outFileChannel.size();

        inFileChannel.transferFrom(outFileChannel,0,size);

        outFile.close();
        inFile.close();
    }

    private static void readFromInput() throws IOException {
        RandomAccessFile file = new RandomAccessFile("D:\\data\\aa.txt", "rw");
        FileChannel channel = file.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        String content = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (!content.equals("q!")){
            content = reader.readLine();
            buffer.put(content.getBytes());
            buffer.flip();
            while (buffer.hasRemaining()){
                channel.write(buffer);
            }
            buffer.clear();
        }
        channel.close();
    }


    private static void readFromStr() throws IOException {
        RandomAccessFile file = new RandomAccessFile("D:\\data\\aa.txt", "rw");
        FileChannel channel = file.getChannel();

        ByteBuffer buffer = ByteBuffer.allocate(1024);
        String data = "hello world";

        buffer.clear();
        buffer.put(data.getBytes());
        buffer.put("\n".getBytes());

        buffer.flip();
        while (buffer.hasRemaining()){
            channel.write(buffer);
        }
        channel.close();
    }


    private static void readFromFile() throws IOException {
        RandomAccessFile rw = new RandomAccessFile("D:\\vbb.xml", "rw");
        FileChannel channel = rw.getChannel();

        ByteBuffer buffer = ByteBuffer.allocate(1024);
        int position = channel.read(buffer);
        while (position != -1){
            buffer.flip();
            while (buffer.hasRemaining()){
                System.out.println((char)buffer.get());
            }
            buffer.clear();
            position = channel.read(buffer);
        }
        channel.close();
        rw.close();
    }
}
