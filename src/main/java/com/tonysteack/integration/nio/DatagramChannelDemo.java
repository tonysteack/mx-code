/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.nio;

import org.junit.Test;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

/**
 * @author tao.qu
 */
public class DatagramChannelDemo {

   @Test
    public void send() throws Exception {
       DatagramChannel datagramChannel = DatagramChannel.open();
       InetSocketAddress address = new InetSocketAddress("127.0.0.1", 9999);
//       datagramChannel.bind(address);

       ByteBuffer sendBuffer = ByteBuffer.wrap("hello world send by server".getBytes(StandardCharsets.UTF_8));
       while (true){
           datagramChannel.send(sendBuffer,address);
           System.out.println("已完成发送");
           TimeUnit.SECONDS.sleep(3);
       }
   }


   @Test
    public void receive() throws Exception{
       DatagramChannel channel = DatagramChannel.open();
       InetSocketAddress address = new InetSocketAddress(9999);
        channel.bind(address);
       ByteBuffer readBuffer = ByteBuffer.allocate(1024);
       while (true){
            readBuffer.clear();
           SocketAddress socketAddress = channel.receive(readBuffer);
           readBuffer.flip();
           System.out.println(socketAddress.toString());
           System.out.println(Charset.forName("UTF-8").decode(readBuffer));
       }
   }
}
