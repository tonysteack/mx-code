package com.tonysteack.integration.mapper;

import com.tonysteack.integration.bean.UniversalDrugDiseaseMapping;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UniversalDrugDiseaseMappingMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UniversalDrugDiseaseMapping record);

    int insertSelective(UniversalDrugDiseaseMapping record);

    UniversalDrugDiseaseMapping selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UniversalDrugDiseaseMapping record);

    int updateByPrimaryKey(UniversalDrugDiseaseMapping record);
}