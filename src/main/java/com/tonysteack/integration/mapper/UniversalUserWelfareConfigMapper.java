package com.tonysteack.integration.mapper;

import com.meditrusthealth.fast.universal.service.db.entity.UniversalUserWelfareConfig;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UniversalUserWelfareConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UniversalUserWelfareConfig record);

    int insertSelective(UniversalUserWelfareConfig record);

    UniversalUserWelfareConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UniversalUserWelfareConfig record);

    int updateByPrimaryKey(UniversalUserWelfareConfig record);

    List<UniversalUserWelfareConfig> selectAll();
}