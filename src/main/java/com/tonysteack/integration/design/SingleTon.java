/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design;

/**
 * @author tao.qu
 * @version 1.0
 **/
//懒汉单例
public class SingleTon {
    private static SingleTon instance = null;

    private SingleTon() {}

    public SingleTon getInstance() {
        if (instance == null) {
            return new SingleTon();
        }
        return instance;
    }
}

//饿汉式单例
class SingleTon1 {
    private static SingleTon1 instance = new SingleTon1();

    private SingleTon1() {}

    public SingleTon1 getInstance() {
        if (instance == null) {
            instance = new SingleTon1();
        }
        return instance;
    }
}

class SingleTon2 {
    private static volatile SingleTon2 instance = null;
    private SingleTon2() {}
    public SingleTon2 getInstance() {
        if (instance == null) {
            synchronized (SingleTon2.class) {
                if (instance == null) {
                    instance = new SingleTon2();
                }
            }
        }
        return instance;
    }
}
