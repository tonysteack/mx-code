/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.facade;

/**
 * 门面模式设计
 *
 * @author tao.qu
 * @version 1.0
 **/
public class FacadeTest {
    public static void main(String[] args) {
        Facade facade = new Facade();
        facade.turnDown();
    }
}

class Facade {
    private TeleVision tv = new TeleVision();
    private Computer pc = new Computer();
    private Conditioner conditioner = new Conditioner();

    void turnDown() {
        tv.turnDown();
        pc.turnDown();
        conditioner.turnDown();
    }
}

class TeleVision {
    void turnDown() {
        System.out.println("关闭电视");
    }
}

class Conditioner {
    void turnDown() {
        System.out.println("关闭冰箱");
    }
}

class Computer {
    void turnDown() {
        System.out.println("关闭电脑");

    }
}