/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.state;

/**
 * @author tao.qu
 * @version 1.0
 **/
public interface State {
    void doJob(Washing washing);
}
