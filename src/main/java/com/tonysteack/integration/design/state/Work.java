/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.state;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class Work implements State{
    @Override
    public void doJob(Washing washing) {
        System.out.println("洗衣机 state:work");
        washing.setState(new End());
        washing.request();
    }
}
