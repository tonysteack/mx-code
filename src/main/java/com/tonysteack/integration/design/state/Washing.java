/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.state;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class Washing {
    private State state = null;

    public void setState(State state){
        this.state = state;
        if (state ==null){
            System.out.println("current state:null");
        }else {
            System.out.println("current state :" + state.getClass().getSimpleName());
        }
    }

    public void request(){
        if (state !=null){
            state.doJob(this);
        }
    }


}
