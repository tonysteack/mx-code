/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.state;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class End implements State{
    @Override
    public void doJob(Washing washing) {
        System.out.println("洗衣机 state:end");
        washing.setState(null);
    }
}
