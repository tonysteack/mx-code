/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.template;

/**
 * @author tao.qu
 * @version 1.0
 **/
public abstract class GoSchool {
    public abstract void getUp();
    public abstract void dress();
    public abstract void wash();

    public void goSchool() {
        getUp();
        dress();
        wash();
    }
}

class Student extends GoSchool {
    @Override
    public void getUp() {
        System.out.println("学生起床了");
    }

    @Override
    public void dress() {
        System.out.println("学生穿衣服了");
    }

    @Override
    public void wash() {
        System.out.println("学生洗漱了，准备去学校");
    }
}

class Teacher extends GoSchool {
    @Override
    public void getUp() {
        System.out.println("老师起床了");
    }

    @Override
    public void dress() {
        System.out.println("老师穿衣服了");
    }

    @Override
    public void wash() {
        System.out.println("老师洗漱了");
    }
}