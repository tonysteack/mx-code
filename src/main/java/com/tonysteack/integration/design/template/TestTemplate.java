/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.template;

/**
 * 模板模式
 * @author tao.qu
 * @version 1.0
 **/
public class TestTemplate {
    public static void main(String[] args) {
        GoSchool student = new Student();
        student.goSchool();
        GoSchool teacher = new Teacher();
        teacher.goSchool();
    }
}
