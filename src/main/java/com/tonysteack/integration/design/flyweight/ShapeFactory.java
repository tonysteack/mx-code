/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.flyweight;

import java.util.HashMap;
import java.util.Map;

public class ShapeFactory {
    private static final Map<String,Shape> map = new HashMap<>();

    public static Shape getCircle(String color){
        Circle circle = (Circle) map.get(color);
        if (null == circle){
            circle = new Circle(color);
            map.put(color,circle);
        }
        return circle;
    }
}
