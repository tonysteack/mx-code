/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.flyweight;

import java.util.Random;

/**
 * @author tao.qu
 */
public class FlyWeightMode {
    private static final String[] colors = new String[]{"black","white","read"};
    private static final Random random = new Random();

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            Circle circle = (Circle) ShapeFactory.getCircle(getRandomColor());

            System.out.println(circle);
        }
    }

    private static String getRandomColor(){
        return colors[(int) Math.random() * 3];
    }

    public static int getX(){
        return ((int) (Math.random() * 100));
    }

    public static int getY(){
        return ((int) (Math.random() * 100));
    }
}
