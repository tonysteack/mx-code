/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.flyweight;

public interface Shape {
    void draw();
}
