/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.flyweight;

import lombok.Data;

/**
 * @author tao.qu
 */
@Data
public class Circle implements Shape{
    private String color;
    private double x;
    private double y;
    private double radius;

    public Circle(String color){
        this.color = color;
    }

    @Override
    public void draw() {
        System.out.println("Circle{" +
                "color='" + color + '\'' +
                ", x=" + x +
                ", y=" + y +
                ", radius=" + radius +
                '}');
    }
}
