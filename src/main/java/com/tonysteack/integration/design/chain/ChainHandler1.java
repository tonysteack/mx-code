/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.chain;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class ChainHandler1 extends ChainHandler{
    @Override
    public void handleEvent(int type) {
        if (type == 1){
            System.out.println("i can resolve num 1");
        }else {
            if (nextHandler != null){
                nextHandler.handleEvent(type);
            }
        }
    }
}
