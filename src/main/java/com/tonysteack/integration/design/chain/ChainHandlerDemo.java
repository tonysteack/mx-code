/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.chain;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class ChainHandlerDemo {

    public static void main(String[] args) {
        int num = 1;
        ChainHandler chainHandler = new ChainHandler2();
        chainHandler.appendHandler(new ChainHandler1())
                .appendHandler(new ChainHandlerOther());

        chainHandler.handleEvent(num);
    }
}
