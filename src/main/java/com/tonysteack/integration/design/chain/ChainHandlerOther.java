/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.chain;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class ChainHandlerOther extends ChainHandler{
    @Override
    public void handleEvent(int type) {
        if (type != 1 && type != 2){
            System.out.println("ican resolve other nums");
        }else {
            if (nextHandler != null) {
                nextHandler.handleEvent(type);
            }
        }
    }
}
