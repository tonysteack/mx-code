/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.chain;

/**
 * @author tao.qu
 * @version 1.0
 **/
public abstract class ChainHandler {
    protected ChainHandler nextHandler;

    public ChainHandler getNextHandler() {
        return nextHandler;
    }

    public void setNextHandler(ChainHandler chainHandler){
        this.nextHandler = chainHandler;
    }

    public ChainHandler appendHandler(ChainHandler chainHandler){
        this.nextHandler = chainHandler;
        return chainHandler;
    }

    public abstract void handleEvent(int type);

}
