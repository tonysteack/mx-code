/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.chain;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class ChainHandler2 extends ChainHandler{
    @Override
    public void handleEvent(int type) {
        if (type == 2){
            System.out.println("i can resolve num 2");
        }else {
            if (nextHandler != null){
                nextHandler.handleEvent(type);
            }
        }
    }
}
