/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.observer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class Blog {
    List<Observer> observerList = new ArrayList<>();

    public void addObserver(Observer observer) {
        observerList.add(observer);
    }

    public void publish() {
        System.out.println("new blog published!");
        Iterator<Observer> iterator = observerList.iterator();
        while (iterator.hasNext()) {
            iterator.next().send();
        }
    }
}
