/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.observer;

/**
 * @author tao.qu
 * @version 1.0
 **/
public interface Observer {
    public void send();
}
