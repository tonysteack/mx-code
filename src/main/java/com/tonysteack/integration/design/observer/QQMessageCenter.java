/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.observer;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class QQMessageCenter implements Observer{
    @Override
    public void send() {
        System.out.println("QQ message send");
    }
}
