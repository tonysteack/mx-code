/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.observer;

import java.util.concurrent.CompletableFuture;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class ObserverTest {
    public static void main(String[] args) {
        Blog blog = new Blog();
        blog.addObserver(new WxMessageCenter());
        blog.addObserver(new QQMessageCenter());
        blog.publish();
        CompletableFuture future = new CompletableFuture<>();
    }
}
