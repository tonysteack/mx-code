/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.unknown;

public class Wxpay implements PayInterface{
    @Override
    public void pay() {
        System.out.println("微信支付");
    }
}
