/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.unknown;

public class UnionPay implements PayInterface{
    @Override
    public void pay() {
        System.out.println("银联支付");
    }
}
