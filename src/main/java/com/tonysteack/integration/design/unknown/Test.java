/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.unknown;

public class Test {
    public static void main(String[] args) {
        PayUtils utils = PayUtils.getInstance();
        utils.pay("ali");
        utils.pay("wx");
    }
}
