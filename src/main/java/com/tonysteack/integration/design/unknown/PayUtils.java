/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.unknown;

import java.util.HashMap;
import java.util.Map;

public class PayUtils {
    private static PayUtils payUtils;
    private static final Map<String,PayInterface> map;
    static {
        map = new HashMap<>();
        map.put("ali",new Alipay());
        map.put("wx",new Wxpay());
        map.put("union",new UnionPay());
    }

    public PayUtils(){}

    public static PayUtils getInstance(){
        if (payUtils == null){
            synchronized (PayUtils.class){
                if (payUtils == null){
                    payUtils = new PayUtils();
                }
            }
        }
        return payUtils;
    }

    public void pay(String type){
        map.get(type).pay();
    }
}
