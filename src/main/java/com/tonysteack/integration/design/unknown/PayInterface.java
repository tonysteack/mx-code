/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.unknown;

public interface PayInterface {
    void pay();
}
