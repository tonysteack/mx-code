/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.observe;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class ObserverDemo {

    public static void main(String[] args) {
        Observers obs = new ConcreateObserver();
        Subject sub = new ConcreateSubject();
        sub.attach(obs);
        sub.setState(2);
        sub.notify("state changed");
    }
}
