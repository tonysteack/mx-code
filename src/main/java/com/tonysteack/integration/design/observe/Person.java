/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.observe;

/**
 * @author tao.qu
 * @version 1.0
 **/
public interface Person {
    boolean isExpectedPrice(int price);

    void shortMsg(String msg);
}
