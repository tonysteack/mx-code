/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.observe;

/**
 * @author tao.qu
 * @version 1.0
 **/
public interface Product {
    void setPrice(int price);
    int getPrice();
    void fllow(Person user);
    void unFlow(Person user);
    void notifyToBuy();
}
