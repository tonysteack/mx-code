/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.observe;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class ConcreateSubject implements Subject{

    private List<Observers> observersList = new ArrayList<>();
    private int state;
    @Override
    public void setState(int state) {
        this.state = state;
        notify("new state="+state);
    }

    @Override
    public int getState() {
        return 0;
    }

    @Override
    public void attach(Observers obs) {
        observersList.add(obs);
    }

    @Override
    public void detach(Observers obs) {
        observersList.remove(obs);
    }

    @Override
    public void notify(String msg) {
        observersList.forEach(
                obs-> obs.update(msg)
        );
    }
}
