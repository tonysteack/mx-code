/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.observe;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author tao.qu
 * @version 1.0
 **/
@Data
@AllArgsConstructor
public class User {
    private int id;
    private String username;
    private String gender;
    private int age;

    public User(String username, String gender, int age) {
        this.username = username;
        this.gender = gender;
        this.age = age;
    }
}
