/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.observe;

/**
 * @author tao.qu
 * @version 1.0
 **/
public interface Subject {
    void setState(int state);
    int getState();
    void attach(Observers obs);
    void detach(Observers obs);
    void notify(String msg);
}
