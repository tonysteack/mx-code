/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.observe;

import java.util.List;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class Laptop implements Product{
    private List<Person> personList;
    private int price;
    @Override
    public void setPrice(int price) {
        this.price = price;
        System.out.println("现在价格 price=" + price);
        notifyToBuy();
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public void fllow(Person user) {
        personList.add(user);
    }

    @Override
    public void unFlow(Person user) {
        personList.remove(user);
    }

    @Override
    public void notifyToBuy() {
        String msg = "" + price;
        personList.forEach(person -> {
            if (person.isExpectedPrice(price)){
                person.shortMsg(msg);
            }
        });
    }
}
