/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.decorder;

/**
 * @author tao.qu
 * @version 1.0
 **/
public abstract class Decorator implements Shape{
    protected Shape circle;

    public Decorator(Shape shape){
        this.circle = shape;
    }

    @Override
    public void draw(){
        circle.draw();
    }
}
