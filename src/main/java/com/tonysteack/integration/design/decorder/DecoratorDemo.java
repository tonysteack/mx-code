/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.decorder;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class DecoratorDemo {

    public static void main(String[] args) {
        Shape shape = new Circle();
        shape.draw();
        Decorator circleEdge = new CircleEdge(shape);
        circleEdge.draw();
        Decorator circleFill = new CircleFill(shape);
        circleFill.draw();
    }
}
