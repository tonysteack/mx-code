/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.decorder;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class CircleEdge extends Decorator{
    public CircleEdge(Shape shape) {
        super(shape);
    }

    private void setCircleEdge(){
        System.out.println("圆的边设置为红色");
    }

    @Override
    public void draw() {
        super.draw();
        setCircleEdge();
    }
}
