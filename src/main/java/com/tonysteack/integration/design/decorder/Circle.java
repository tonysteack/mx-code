/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.decorder;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class Circle implements Shape{
    @Override
    public void draw() {
        System.out.println("画了一个圆");
    }
}
