/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.decorder;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class CircleFill extends Decorator{
    public CircleFill(Shape shape) {
        super(shape);
    }

    private void circleFill(){
        System.out.println("圆被填充了颜色");
    }

    @Override
    public void draw() {
        super.draw();
        circleFill();
    }
}
