/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.factory;

/**
 * @author tao.qu
 * @version 1.0
 **/
public interface AbstractCarFactory {
    MiniCar produceMiniCar();

    SuvCar produceSuvCar();
}

class AudiFactory implements AbstractCarFactory {
    public AudiFactory() {
        super();
    }

    @Override
    public MiniCar produceMiniCar() {
        return new AudiMiniCar();
    }

    @Override
    public SuvCar produceSuvCar() {
        return new AudiSuvCar();
    }
}

class BenzFactory implements AbstractCarFactory {
    @Override
    public MiniCar produceMiniCar() {
        return new BenzMiniCar();
    }

    @Override
    public SuvCar produceSuvCar() {
        return new BenZSuvCar();
    }
}
