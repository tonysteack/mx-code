/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.factory;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class AudiMiniCar implements MiniCar{
    @Override
    public void showInfo() {
        System.out.println("这是一辆奥迪迷你小汽车");
    }
}
