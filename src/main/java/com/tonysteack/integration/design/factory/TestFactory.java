/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.factory;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class TestFactory {
    public static void main(String[] args) {
//        CarFactory bmw = new BmwCarFactory();
//        bmw.produceCar().showInfo();
        AbstractCarFactory benzFactory = new BenzFactory();
        benzFactory.produceMiniCar().showInfo();
        benzFactory.produceSuvCar().showInfo();
    }
}
