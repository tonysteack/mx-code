/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.factory;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class DazhongCarFactory implements CarFactory{
    @Override
    public Car produceCar() {
        return new DaZhongCar();
    }
}
