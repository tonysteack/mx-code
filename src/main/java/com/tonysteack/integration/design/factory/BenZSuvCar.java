/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.factory;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class BenZSuvCar implements SuvCar{
    @Override
    public void showInfo() {
        System.out.println("这是一辆奔驰SUV汽车");
    }
}
