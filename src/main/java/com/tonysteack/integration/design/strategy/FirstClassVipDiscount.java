/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.strategy;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class FirstClassVipDiscount implements DiscountStrategy{
    @Override
    public double calculateDiscount(double originPrice) {
        return Math.round(0.7 * originPrice);
    }
}
