/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.strategy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class DiscountContext {
    private static Map<String, DiscountStrategy> strategyMap = new HashMap<>();
    static {
        strategyMap.put("normal", new NormalUserDiscount());
        strategyMap.put("firstClass", new FirstClassVipDiscount());
        strategyMap.put("super", new SuperVipDiscount());
    }

    public static DiscountStrategy getDiscount(String userType) {
        return strategyMap.getOrDefault(userType, new NormalUserDiscount());
    }
}
