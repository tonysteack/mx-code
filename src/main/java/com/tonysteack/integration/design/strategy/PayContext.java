/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.strategy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class PayContext {
    private static Map<String,Pay> payMap = new HashMap<>();

    static {
        payMap.put(PayEnum.ALIPAY.getPayType(), new ALiPay());
        payMap.put(PayEnum.WECHAT.getPayType(), new WechatPay());
    }

    public static void main(String[] args) {
        String payType = "WECHAT";
        Pay pay = payMap.get(payType);
        pay.pay();
    }
}
