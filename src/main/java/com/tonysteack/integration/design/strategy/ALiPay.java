/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.strategy;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class ALiPay implements Pay{
    @Override
    public String payType(String type) {
        return PayEnum.ALIPAY.getPayType();
    }

    @Override
    public void pay() {
        System.out.println("用户选择了阿里支付");
    }
}
