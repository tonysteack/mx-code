/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.strategy;

/**
 * @author tao.qu
 */
public interface strategy {
    int doOperation(int num1, int num2);
}
