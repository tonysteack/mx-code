/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.strategy;

public class Context {
    private strategy strategy;

    public Context(com.tonysteack.integration.design.strategy.strategy strategy) {
        this.strategy = strategy;
    }

    public void calculate(int num1,int num2){
        System.out.println(strategy.doOperation(num1, num2));
    }

    public static void main(String[] args) {
        Context context;
        com.tonysteack.integration.design.strategy.strategy oper;
        oper = new OperationAdd();
        context = new Context(oper);
        context.calculate(10,12);

        oper = new OpertationSubtract();
        context = new Context(oper);
        context.calculate(9,5);

        oper = new OpertationMultiply();
        context = new Context(oper);
        context.calculate(31,28);
    }
}
