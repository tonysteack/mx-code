/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.strategy;

/**
 * @author tao.qu
 * @version 1.0
 **/
public interface DiscountStrategy {
    double calculateDiscount(double originPrice);
}
