/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.strategy;

/**
 * @author tao.qu
 * @version 1.0
 **/
public class NormalUserDiscount implements DiscountStrategy {
    @Override
    public double calculateDiscount(double originPrice) {
        return originPrice;
    }
}


class Singleton {
    private static volatile Singleton instance;

    private Singleton() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
    }

    public Singleton getInstance() {
        return instance;
    }
}
