/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.design.strategy;

/**
 * @author tao.qu
 * @version 1.0
 **/
public enum PayEnum {
    WECHAT("WECHAT", "微信支付"),
    ALIPAY("ALIPAY", "支付宝支付");

    private String payType;
    private String message;

    PayEnum(String payType, String message) {
        this.payType = payType;
        this.message = message;
    }

    public String getPayType() {
        return payType;
    }

    public String getMessage() {
        return message;
    }

    public static String getMessageByPayType(String payType) {
        if (payType == null) {
            System.out.println("paytype is null");
            return null;
        }
        PayEnum[] enums = PayEnum.values();
        for (PayEnum payEnum : enums) {
            if (payEnum.getPayType().equals(payType)) {
                return payEnum.getMessage();

            }
        }
        return null;
    }
}
