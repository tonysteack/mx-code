/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * @author tao.qu
 * @version 1.0
 **/
@Getter
@AllArgsConstructor
public enum FileSuffixEnum {
    SYSTEM_IMAGE("SYSTEM_IMAGE", "系统镜像", "iso"),
    JPEG_IMAGE("JPEG_IMAGE", "静态图片JPEG格式", "jpeg"),
    GIF_IMAGE("GIF_IMAGE", "动态图片", "gif"),
    ZIP("ZIP", "ZIP压缩包", "zip"),
    TAR("TAR", "TAR压缩包", "tar"),
    ;
    private String code;
    private String description;
    private String suffix;

    /**
     * 根据文件后缀名获取文件类型
     *
     * @param suffix
     * @return
     */
    public static String getCodeBySuffix(String suffix) {
        Objects.requireNonNull(suffix, "文件后缀不可为空");
        FileSuffixEnum[] enums = FileSuffixEnum.values();
        for (FileSuffixEnum suffixEnum : enums) {
            if (suffix.compareToIgnoreCase(suffixEnum.getSuffix()) == 0) {
                return suffixEnum.getCode();
            }
        }
        return null;
    }
}
