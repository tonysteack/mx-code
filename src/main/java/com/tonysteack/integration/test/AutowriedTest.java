/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.test;

import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

/**
 * 测试自动注入
 * @author tao.qu
 * @version 1.0
 **/
@Import(Handler.class)
public class AutowriedTest {

}

