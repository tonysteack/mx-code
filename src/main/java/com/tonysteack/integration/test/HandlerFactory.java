/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tao.qu
 * @version 1.0
 **/
@Component
public class HandlerFactory {
//    @Qualifier("handlerMap")
    @Resource
    private Map<String, Handler> handlerMap;

    public Handler of(String type) {
        return handlerMap.get(type);
    }

    @Configuration
    static class HandlerConfig {
        @Resource
        private List<Handler> handlers;

        @Bean("handlerMap")
        public Map<String, Handler> handlerMap() {
            Map<String, Handler> handlerMap = new HashMap<>();
            handlers.forEach(handler -> handlerMap.putIfAbsent(handler.getType(), handler));
            return handlerMap;
        }
    }
}
