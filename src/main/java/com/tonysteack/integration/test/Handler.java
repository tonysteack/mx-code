/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.test;

/**
 * @author tao.qu
 * @version 1.0
 **/
public interface Handler {
    String getType();

    void handler(String handlerName);
}
