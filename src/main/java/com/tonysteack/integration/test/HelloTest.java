/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.test;

import lombok.ToString;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//@Import(HelloTest.User.class)
@Import(MyImportSelector.class)
/**
 * @author tao.qu
 */
public class HelloTest {
    private static Map<String, String> map;
    static {
        map = new ConcurrentHashMap<>();
    }

    @ToString
    static class User {
        private Integer age;
        private String name;

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static void main(String[] args) {
//        String path = PathUtils.getProjectRootPath();
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(HelloTest.class);
        User user = context.getBean(User.class);
        System.out.println(user);
    }
}

class MyImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{"com.tonysteack.integration.test.HelloTest.User"};
    }
}