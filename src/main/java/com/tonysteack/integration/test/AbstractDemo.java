/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.test;

/**
 * @author tao.qu
 * @version 1.0
 **/
public abstract class AbstractDemo {
    abstract int calculate(int num, int num1);

    void print(int num1, int num2) {
        System.out.println(this.calculate(num1, num2));
        System.out.println("test abstract class");
    }
}

class Add extends AbstractDemo {
    @Override
    int calculate(int num, int num1) {
        return num1 + num;
    }
}

class Multiply extends AbstractDemo {
    @Override
    int calculate(int num, int num1) {
        return num * num1;
    }
}

class Devide extends AbstractDemo {
    @Override
    int calculate(int num, int num1) {
        return num - num1;
    }
}

class Test1 {
    public static void main(String[] args) {
        AbstractDemo demo = new Add();
        demo.print(1,2);
    }
}