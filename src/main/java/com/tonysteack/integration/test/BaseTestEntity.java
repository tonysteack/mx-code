/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.test;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class BaseTestEntity {
    private String name;
}

@ToString(callSuper = true)
@Data
class Entity1 extends BaseTestEntity{
    private Integer age;
}

@ToString(callSuper = true)
@Data
class Entity2 extends BaseTestEntity{
    private String address;
}


class Test{
    private static void print(BaseTestEntity entity){
        System.out.println(entity.toString());
    }

    public static void main(String[] args) {
        Entity1 entity1 = new Entity1();
        entity1.setAge(18);
        entity1.setName("zhangsan");
        print(entity1);
    }
}