/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.test;

import org.springframework.stereotype.Component;

/**
 * @author tao.qu
 * @version 1.0
 **/
@Component
class EventHandler implements Handler {
    @Override
    public String getType() {
        return "event";
    }

    @Override
    public void handler(String handlerName) {
        System.out.println(handlerName);
    }
}
