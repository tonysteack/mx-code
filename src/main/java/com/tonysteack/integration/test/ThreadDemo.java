/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.test;

/**
 * 多线程测试
 * @author tao.qu
 * @version 1.0
 **/
public class ThreadDemo {
    //先使用单线程对数据进行修改，再使用多线程对数据修改，测试线程安全问题
    static class test{
        int i = 50;
        int j = 0;
        public void update() {
            i = 100;
        }

        public int get() {
            j = i;
            j++;
            return j;
        }

        public static void main(String[] args) {
            test t = new test();
            t.update();
            System.out.println(t.get());

            for (int i = 0; i < 10; i++) {
                new Thread(()-> {
                    test t1 = new test();
                    t1.update();
                    System.out.println(Thread.currentThread().getName() +t1.get());
                }).start();
            }
        }
    }
}
