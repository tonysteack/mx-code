/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author tao.qu
 * @version 1.0
 **/
@Component
@ServerEndpoint("/beacon")
@Slf4j
public class WebSocketServer {

    private Session session;

    private static CopyOnWriteArraySet<WebSocketServer>  websocketSet = new CopyOnWriteArraySet<>();

    @OnOpen
    public void onOpen(Session session) {
        this.session = session;
        websocketSet.add(this);
        log.info("[websocket] 有新的连接进入,当前连接数量={}",websocketSet.size());
    }

    @OnClose
    public void onClose() {
        websocketSet.remove(this);
        log.info("[websocket] 本连接退出,当前连接数量={}",websocketSet.size());
    }

    @OnMessage
    public void OnMessage(String message) {
        log.info("[websocket] recieve message ={}", message);
    }

    public void sendMessage(String message) {
        for (WebSocketServer socket : websocketSet) {
            log.info("[websocket消息] 发送消息={}", message);
            try {
                socket.session.getBasicRemote().sendText(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
