/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.controller;

import com.tonysteack.integration.service.HelloService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class HelloController {
    @Resource
    private HelloService helloService;

    @Resource
    private WebSocketServer webSocketServer;

    @GetMapping("/hello")
    public String hello(@RequestParam("msg") String message) {
        webSocketServer.sendMessage("hello world");
        return helloService.hello(message);
    }
}
