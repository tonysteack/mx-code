/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.controller;

/**
 * @author tao.qu
 * @version 1.0
 **/

import com.tonysteack.integration.utils.CmdUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@RestController
@Slf4j
public class ImageParseController {

    @PostMapping("/image/parse")
    public String parseImage(@RequestParam("file") MultipartFile file) throws IOException {
        String originalFilename = file.getOriginalFilename();
        File saveFile = new File(System.getProperty("user.dir") + "\\" + originalFilename);
        if (!saveFile.exists()) {
            saveFile.createNewFile();
        }
        file.transferTo(saveFile);
        String cmd = String.format("tesseract %s stdout -l %s", System.getProperty("user.dir") + "\\" + originalFilename, "chi_sim");
        String result = CmdUtils.executeCommand(cmd);
        return result;
    }
}
