///**
// * www.meditrusthealth.com Copyright © MediTrust Health 2017
// */
//package com.tonysteack.integration.wc;
//
//import org.apache.flink.api.common.functions.FlatMapFunction;
//import org.apache.flink.api.java.tuple.Tuple2;
//import org.apache.flink.util.Collector;
//
//public class MyFlagMap implements FlatMapFunction<String,Tuple2<String,Integer>> {
//
//    @Override
//    public void flatMap(String s, Collector<Tuple2<String, Integer>> collector) throws Exception {
//        String[] s1 = s.split(" ");
//        for (String s2 : s1) {
//            collector.collect(new Tuple2<>(s2,1));
//        }
//    }
//}
