///**
// * www.meditrusthealth.com Copyright © MediTrust Health 2017
// */
//package com.tonysteack.integration.wc;
//
//import lombok.Getter;
//import lombok.Setter;
//import org.apache.flink.api.common.functions.FlatMapFunction;
//import org.apache.flink.api.common.functions.ReduceFunction;
//import org.apache.flink.api.java.ExecutionEnvironment;
//import org.apache.flink.api.java.operators.DataSource;
//import org.apache.flink.api.java.tuple.Tuple2;
//import org.apache.flink.api.java.tuple.Tuple3;
//import org.apache.flink.streaming.api.datastream.DataStream;
//import org.apache.flink.streaming.api.datastream.DataStreamSource;
//import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
//import org.apache.flink.util.Collector;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class WordCount {
//    private static final String hostname = "192.168.200.101";
//    private static final int port = 9999;
//    private static StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
//    private static final DataStreamSource<String> stream;
//    static {
//        stream = env.socketTextStream(hostname,port,"\n");
//    }
//    public static void main(String[] args) throws Exception {
//        streamFile1();
////        readFile("D:\\code\\test\\src\\main\\resources\\test.text");
////        fromCollection();
//    }
//
//    private static void readFile(String path) throws Exception {
//        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
//        DataSource<String> file = env.readTextFile(path);
//        file.flatMap(new MyFlagMap())
//                .groupBy(0)
//                .sum(1).print();
//    }
//
//    private static void streamFile() throws Exception {
//        stream.flatMap((FlatMapFunction<String, WordWithCount>) (str, collector) -> {
//            for (String value : str.split("\\s")) {
//                collector.collect(new WordWithCount(value,1));
//            }
//        }).keyBy((WordWithCount::getCount))
//                .reduce((ReduceFunction<WordWithCount>) (wordWithCount, t1) -> new WordWithCount(wordWithCount.word, wordWithCount.count+ t1.count));
//        stream.print().setParallelism(1);
//        env.execute("socket wordCount");
//    }
//
//    private static void streamFile1() throws Exception {
//        DataStream<Tuple2<String,Integer>> stream = env.socketTextStream(hostname, port)
//                .flatMap(new Spitter())
//                .keyBy(val->val.f0)
////                .window(TumblingProcessingTimeWindows.of(Time.milliseconds(5)))
//                .reduce((ReduceFunction<Tuple2<String, Integer>>) (val,val1)-> new Tuple2<>(val.f0,val1.f1+val.f1))
//
//                ;
//        stream.print();
//
//        env.execute("window wordCount");
//    }
//
//    public static void fromCollection() throws Exception {
//        List<Tuple3<String, String, Integer>> data = new ArrayList<>();
//        data.add(new Tuple3<>("男","老王",23));
//        data.add(new Tuple3<>("男","老李",22));
//        data.add(new Tuple3<>("男","老马",25));
//        data.add(new Tuple3<>("男","老刘",15));
//
//        env.fromCollection(data)
//                .keyBy(0).min(2)
//                .print();
//
//        env.execute("print max age person");
//
//    }
//
//    @Getter
//    @Setter
//    static class WordWithCount{
//        private String word;
//        private int count;
//        public WordWithCount(){}
//
//        public WordWithCount(String word,int count){
//            this.word = word;
//            this.count = count;
//        }
//    }
//
//    static class Spitter implements FlatMapFunction<String,Tuple2<String,Integer>>{
//
//        @Override
//        public void flatMap(String s, Collector<Tuple2<String, Integer>> collector) throws Exception {
//            for (String s1 : s.split(" ")) {
//                collector.collect(new Tuple2<>(s1,1));
//            }
//        }
//    }
//}
