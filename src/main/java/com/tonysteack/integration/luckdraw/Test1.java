/**
 * www.meditrusthealth.com Copyright © MediTrust Health 2017
 */
package com.tonysteack.integration.luckdraw;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * @author tao.qu
 */
public class Test1 {

    @Getter
    @Setter
    @AllArgsConstructor
    static class Award{
        private String awardName;
        private double probability;
    }

    public static void main(String[] args) {
        List<Award> awardList = new ArrayList<>();
        awardList.add(new Award("手机",0));
        awardList.add(new Award("鼠标",0.01));
        awardList.add(new Award("鼠标垫",0.04));
        awardList.add(new Award("5元红包",0.05));
        awardList.add(new Award("0.5元微信红包",0.2));
        awardList.add(new Award("擦肩而过",0.7));

        Random random = new Random();
        int a=0,b=0,c=0,d=0,e=0,f=0;
        for (int i = 0; i < 1000000; i++) {
            double select = random.nextDouble();
            System.out.println(select);
            String award = getAward(select, awardList);
            System.out.println(award);
            if (null==award){
                award = "擦肩而过";
            }
            switch (Objects.requireNonNull(award)) {
                case "手机":
                    a++;
                    break;
                case "鼠标":
                    b++;
                    break;
                case "鼠标垫":
                    c++;
                    break;
                case "5元红包":
                    d++;
                    break;
                case "0.5元微信红包":
                    e++;
                    break;
                case "擦肩而过":
                    f++;
                    break;
                default: break;
            }
        }
        System.out.println("手机抽中次数是："+a);
        System.out.println("鼠标抽中次数是："+b);
        System.out.println("鼠标垫抽中次数是："+c);
        System.out.println("5元红包抽中次数是："+d);
        System.out.println("0.5元红包抽中次数是："+e);
        System.out.println("擦肩而过抽中次数是："+f);
    }

    public static String getAward(double select,List<Award> list){
        for (Award award : list) {
            if (award.getProbability()>select){
                return award.getAwardName();
            }
        }
        return null;
    }
}
