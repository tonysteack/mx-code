package com.meditrusthealth.fast.universal.service.db.entity;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p> 
 * 用户福利配置表 universal_user_welfare_config
 * </p> 
 * 
 * @author tao.qu 2022-03-22 17:45:52 446
 */
@ToString(callSuper = true)
public class UniversalUserWelfareConfig implements Serializable {
    /**
     * id 描述:福利ID
     */
    private Long id;

    /**
     * drug_name 描述:药品名称
     */
    private String drugName;

    /**
     * project_name 描述:项目名称
     */
    private String projectName;

    /**
     * welfare_type 描述:福利类型
     */
    private String welfareType;

    /**
     * related_disease 描述:关联疾病
     */
    private String relatedDisease;

    /**
     * welfare_amount 描述:优惠额度
     */
    private BigDecimal welfareAmount;

    /**
     * welfare_tag 描述:福利标签
     */
    private String welfareTag;

    /**
     * drug_common_name 描述:药品通用
     */
    private String drugCommonName;

    /**
     * drug_spec 描述:规格信息
     */
    private String drugSpec;

    /**
     * project_desc 描述:项目介绍
     */
    private String projectDesc;

    /**
     * project_id 描述:关联项目ID
     */
    private String projectId;

    /**
     * sort_flag 描述:排序标志 值越大优先级越高
     */
    private Integer sortFlag;

    /**
     * online_status 描述:上下架状态
     */
    private Boolean onlineStatus;

    /**
     * online_time 描述:上架时间
     */
    private Date onlineTime;

    /**
     * offline_time 描述:下架时间
     */
    private Date offlineTime;

    /**
     * thumb_image 描述:封面图地址
     */
    private String thumbImage;

    /**
     * related_drug 描述:关联药品
     */
    private String relatedDrug;

    /**
     * redirect_url 描述:跳转地址
     */
    private String redirectUrl;

    /**
     * create_time 描述:创建时间
     */
    private Date createTime;

    /**
     * update_time 描述:更新时间
     */
    private Date updateTime;

    /**
     * creator 描述:创建人
     */
    private String creator;

    /**
     * modifier 描述:更新人
     */
    private String modifier;

    /**
     * del_flag 描述:删除标识 1 删除 0 未删除
     */
    private Boolean delFlag;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = (drugName == null || drugName.trim().isEmpty() ) ? null : drugName.trim();
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = (projectName == null || projectName.trim().isEmpty() ) ? null : projectName.trim();
    }

    public String getWelfareType() {
        return welfareType;
    }

    public void setWelfareType(String welfareType) {
        this.welfareType = (welfareType == null || welfareType.trim().isEmpty() ) ? null : welfareType.trim();
    }

    public String getRelatedDisease() {
        return relatedDisease;
    }

    public void setRelatedDisease(String relatedDisease) {
        this.relatedDisease = (relatedDisease == null || relatedDisease.trim().isEmpty() ) ? null : relatedDisease.trim();
    }

    public BigDecimal getWelfareAmount() {
        return welfareAmount;
    }

    public void setWelfareAmount(BigDecimal welfareAmount) {
        this.welfareAmount = welfareAmount;
    }

    public String getWelfareTag() {
        return welfareTag;
    }

    public void setWelfareTag(String welfareTag) {
        this.welfareTag = (welfareTag == null || welfareTag.trim().isEmpty() ) ? null : welfareTag.trim();
    }

    public String getDrugCommonName() {
        return drugCommonName;
    }

    public void setDrugCommonName(String drugCommonName) {
        this.drugCommonName = (drugCommonName == null || drugCommonName.trim().isEmpty() ) ? null : drugCommonName.trim();
    }

    public String getDrugSpec() {
        return drugSpec;
    }

    public void setDrugSpec(String drugSpec) {
        this.drugSpec = (drugSpec == null || drugSpec.trim().isEmpty() ) ? null : drugSpec.trim();
    }

    public String getProjectDesc() {
        return projectDesc;
    }

    public void setProjectDesc(String projectDesc) {
        this.projectDesc = (projectDesc == null || projectDesc.trim().isEmpty() ) ? null : projectDesc.trim();
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = (projectId == null || projectId.trim().isEmpty() ) ? null : projectId.trim();
    }

    public Integer getSortFlag() {
        return sortFlag;
    }

    public void setSortFlag(Integer sortFlag) {
        this.sortFlag = sortFlag;
    }

    public Boolean getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(Boolean onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public Date getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(Date onlineTime) {
        this.onlineTime = onlineTime;
    }

    public Date getOfflineTime() {
        return offlineTime;
    }

    public void setOfflineTime(Date offlineTime) {
        this.offlineTime = offlineTime;
    }

    public String getThumbImage() {
        return thumbImage;
    }

    public void setThumbImage(String thumbImage) {
        this.thumbImage = (thumbImage == null || thumbImage.trim().isEmpty() ) ? null : thumbImage.trim();
    }

    public String getRelatedDrug() {
        return relatedDrug;
    }

    public void setRelatedDrug(String relatedDrug) {
        this.relatedDrug = (relatedDrug == null || relatedDrug.trim().isEmpty() ) ? null : relatedDrug.trim();
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = (redirectUrl == null || redirectUrl.trim().isEmpty() ) ? null : redirectUrl.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = (creator == null || creator.trim().isEmpty() ) ? null : creator.trim();
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = (modifier == null || modifier.trim().isEmpty() ) ? null : modifier.trim();
    }

    public Boolean getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Boolean delFlag) {
        this.delFlag = delFlag;
    }
}